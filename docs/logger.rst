======================
Webwhois log structure
======================

This document describes structure of logs sent to FRED logger service.

* All messages belong to service ``WebWhois``
* Return code is either ``Ok``, ``NotFound`` or ``Error`` (default)

.. contents:: Table of Contents
   :depth: 2

The following abbreviations are used to describe references and properties:

* ``R`` – property/reference is required
* ``#`` – number of properties/references in log entry; ``*`` stands for arbitrary number
* ``key`` – property/reference key
* ``type`` – property/reference type
* ``note`` – anything else that's worth mentioning

``Info``
========

Information about an object was retrieved.

References
----------

.. csv-table::
   :header: R, #, key, type, note

   , 1, contact, str, Contact identifier
   , 1, domain, str, Domain identifier
   , 1, keyset, str, Keyset identifier
   , 1, nsset, str, Nsset identifier
   , 1, registrar, str, Registrar identifier

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, note

   I, ✔, 1, handle, str, The object identifier
   I, ✔, 1, handleType, str, "The object type searched - one of 'contact', 'domain', 'keyset', 'nsset', 'registrar', 'multiple'"
   O,  , 1, exception, str, The type of exception which caused the error.

``RecordStatement``
===================

Record statement for an object was downloaded.

References
----------

None

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, note

   I, ✔, 1, handle, str, The object identifier
   I, ✔, 1, objectType, str, "The object type searched - one of 'contact', 'domain', 'keyset', 'nsset'"
   I, ✔, 1, documentType, Literal['public'],
   O,  , 1, exception, str, The type of exception which caused the error.

``ScanResults``
===============

Domain scan results were provided.

References
----------

None

Properties
----------

.. csv-table::
   :header: I/O, R, #, key, type, note

   I, ✔, 1, domain, str, The domain identifier
