#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from contextlib import suppress
from typing import Any, Dict, cast

from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.urls import ResolverMatch, reverse
from django.utils.translation import gettext as _
from fred_types import RegistryObjectType
from grill import References
from regal import DomainLifeCycleStage
from regal.exceptions import ContactDoesNotExist, KeysetDoesNotExist, NssetDoesNotExist, RegistrarDoesNotExist

from ..constants import REGISTRAR
from ..exceptions import WebwhoisError
from ..settings import CONTACT_CLIENT, DOMAIN_CLIENT, KEYSET_CLIENT, NSSET_CLIENT, REGISTRAR_CLIENT
from .base import RegistryObjectView


class ResolveHandleTypeView(RegistryObjectView[Dict[str, Any]]):
    """Find all objects with the handle."""

    object_type = "multiple"
    template_name = "webwhois/multiple_entries.html"

    def _get_object(self, handle: str) -> Dict[str, Any]:
        objects: Dict[str, Any] = {}
        with suppress(ContactDoesNotExist):
            objects[RegistryObjectType.CONTACT] = CONTACT_CLIENT.get_contact_info(CONTACT_CLIENT.get_contact_id(handle))
        with suppress(NssetDoesNotExist):
            objects[RegistryObjectType.NSSET] = NSSET_CLIENT.get_nsset_info(NSSET_CLIENT.get_nsset_id(handle))
        with suppress(KeysetDoesNotExist):
            objects[RegistryObjectType.KEYSET] = KEYSET_CLIENT.get_keyset_info(KEYSET_CLIENT.get_keyset_id(handle))
        with suppress(RegistrarDoesNotExist):
            objects[REGISTRAR] = REGISTRAR_CLIENT.get_registrar_info(handle)

        result = DOMAIN_CLIENT.get_life_cycle_stage(handle)
        if result.stage == DomainLifeCycleStage.REGISTERED:
            objects[RegistryObjectType.DOMAIN] = DOMAIN_CLIENT.get_domain_info(result.id)
        elif result.stage == DomainLifeCycleStage.DELETE_CANDIDATE:
            # Obscure information about delete candidates.
            objects[RegistryObjectType.DOMAIN] = {"fqdn": self.kwargs['handle']}

        if not objects:
            raise WebwhoisError(handle, 'OBJECT_NOT_FOUND', message=_("Record not found"))
        return objects

    def _get_references(self, obj: Dict[str, Any]) -> References:
        """Return log references for the object."""
        references: Dict[str, Any] = {}
        if RegistryObjectType.CONTACT in obj:
            references[RegistryObjectType.CONTACT] = obj[RegistryObjectType.CONTACT].contact_id
        if hasattr(obj.get(RegistryObjectType.DOMAIN), 'domain_id'):
            references[RegistryObjectType.DOMAIN] = obj[RegistryObjectType.DOMAIN].domain_id
        if RegistryObjectType.KEYSET in obj:
            references[RegistryObjectType.KEYSET] = obj[RegistryObjectType.KEYSET].keyset_id
        if RegistryObjectType.NSSET in obj:
            references[RegistryObjectType.NSSET] = obj[RegistryObjectType.NSSET].nsset_id
        if REGISTRAR in obj:
            references[REGISTRAR] = obj[REGISTRAR].registrar_id
        return references

    def _make_context(self, obj: Dict[str, Any]) -> Dict[str, Any]:
        """Turn object into a context."""
        return dict(obj.items())

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        obj = self.get_object()
        if len(obj) == 1:
            url = reverse("webwhois:detail_{}".format(tuple(obj)[0]), kwargs={"handle": self.kwargs["handle"]},
                          current_app=cast(ResolverMatch, self.request.resolver_match).namespace)
            return HttpResponseRedirect(url)
        return super().get(request, *args, **kwargs)
