#
# Copyright (C) 2017-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
import logging
from typing import Any, Dict, Optional, Type, cast

from django.core.cache import cache
from django.forms import Form
from django.http import Http404, HttpRequest, HttpResponse, HttpResponseRedirect
from django.urls import ResolverMatch, reverse
from django.utils.encoding import force_str
from django.utils.html import format_html
from django.utils.translation import get_language, gettext_lazy as _
from django.views.generic import TemplateView, View
from fred_idl.Registry.PublicRequest import (
    HAS_DIFFERENT_BLOCK,
    INVALID_EMAIL,
    OBJECT_ALREADY_BLOCKED,
    OBJECT_NOT_BLOCKED,
    OBJECT_NOT_FOUND,
    OBJECT_TRANSFER_PROHIBITED,
    OPERATION_PROHIBITED,
    LockRequestType,
)
from fred_types import RegistryObjectType
from omniORB import EnumItem

from webwhois.forms import BlockObjectForm, PersonalInfoForm, SendPasswordForm, UnblockObjectForm
from webwhois.forms.public_request import (
    CONFIRMATION_METHOD_IDL_MAP,
    LOCK_TYPE_ALL,
    LOCK_TYPE_TRANSFER,
    LOCK_TYPE_URL_PARAM,
    SEND_TO_CUSTOM,
    SEND_TO_IN_REGISTRY,
)
from webwhois.forms.widgets import DeliveryType
from webwhois.utils.corba_wrapper import PUBLIC_REQUEST
from webwhois.utils.public_response import BlockResponse, PersonalInfoResponse, PublicResponse, SendPasswordResponse
from webwhois.views.public_request_mixin import FormType, PublicRequestFormView, PublicRequestKnownException

from ..constants import PublicRequestsLogEntryType
from ..settings import SECRETARY_CLIENT

WEBWHOIS_LOGGING = logging.getLogger(__name__)


class SendPasswordFormView(PublicRequestFormView[SendPasswordForm]):
    """Send password (AuthInfo) view."""

    form_class = SendPasswordForm
    template_name = 'webwhois/form_send_password.html'
    form_cleaned_data = None  # type: Dict[str, Any]

    def _call_registry_command(self, form: SendPasswordForm, log_request_id: int) -> int:
        data = form.cleaned_data
        try:
            if data['send_to'].choice == 'custom_email':
                response_id = PUBLIC_REQUEST.create_authinfo_request_non_registry_email(
                    self._get_object_type(data['object_type']), data['handle'], log_request_id,
                    CONFIRMATION_METHOD_IDL_MAP[data['confirmation_method']], data['send_to'].custom_email)
            else:
                # confirm_type_name is 'signed_email'
                response_id = PUBLIC_REQUEST.create_authinfo_request_registry_email(
                    self._get_object_type(data['object_type']), data['handle'], log_request_id)
        except OBJECT_NOT_FOUND as err:
            form.add_error('handle',
                           _('Object not found. Check that you have correctly entered the Object type and Handle.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except OBJECT_TRANSFER_PROHIBITED as err:
            form.add_error('handle', _('Transfer of object is prohibited. The request can not be accepted.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except INVALID_EMAIL as err:
            form.add_error('send_to', _('The email was not found or the address is not valid.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        return cast(int, response_id)

    def get_public_response(self, form: SendPasswordForm, public_request_id: int) -> SendPasswordResponse:
        if form.cleaned_data['send_to'].choice == 'custom_email':
            custom_email = form.cleaned_data['send_to'].custom_email
        else:
            custom_email = None
        return SendPasswordResponse(form.cleaned_data['object_type'], public_request_id, form.log_entry_type,
                                    form.cleaned_data['handle'], custom_email, form.cleaned_data['confirmation_method'])

    def get_initial(self) -> Dict[str, Any]:
        data = super().get_initial()
        data["handle"] = self.request.GET.get("handle")
        data["object_type"] = self.request.GET.get("object_type")
        send_to = self.request.GET.get("send_to")
        if send_to and send_to in (SEND_TO_IN_REGISTRY, SEND_TO_CUSTOM):
            data["send_to"] = DeliveryType(send_to, '')
        return data

    def get_success_url(self) -> str:
        if self.success_url:
            return force_str(self.success_url)
        url_name = "webwhois:response_not_found"
        if self.form_cleaned_data['send_to'].choice == 'email_in_registry':
            url_name = 'webwhois:email_in_registry_response'
        else:
            assert self.form_cleaned_data['send_to'].choice == 'custom_email'  # noqa: S101
            url_name = 'webwhois:public_response'
        return reverse(url_name, kwargs={'public_key': self.public_key},
                       current_app=cast(ResolverMatch, self.request.resolver_match).namespace)


class PersonalInfoFormView(PublicRequestFormView[PersonalInfoForm]):
    """View to create public request for personal info."""

    form_class = PersonalInfoForm
    template_name = 'webwhois/form_personal_info.html'
    form_cleaned_data = None  # type: Dict[str, Any]

    def _call_registry_command(self, form: PersonalInfoForm, log_request_id: int) -> int:
        data = form.cleaned_data
        try:
            if data['send_to'].choice == SEND_TO_CUSTOM:
                response_id = PUBLIC_REQUEST.create_personal_info_request_non_registry_email(
                    data['handle'], log_request_id, CONFIRMATION_METHOD_IDL_MAP[data['confirmation_method']],
                    data['send_to'].custom_email)
            else:
                assert data['send_to'].choice == SEND_TO_IN_REGISTRY  # noqa: S101
                response_id = PUBLIC_REQUEST.create_personal_info_request_registry_email(data['handle'], log_request_id)
        except OBJECT_NOT_FOUND as err:
            form.add_error('handle',
                           _('Object not found. Check that you have correctly entered the contact handle.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except INVALID_EMAIL as err:
            form.add_error('send_to', _('The email was not found or the address is not valid.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        return cast(int, response_id)

    def get_public_response(self, form: PersonalInfoForm, public_request_id: int) -> PersonalInfoResponse:
        if form.cleaned_data['send_to'].choice == 'custom_email':
            custom_email = form.cleaned_data['send_to'].custom_email
        else:
            custom_email = None
        return PersonalInfoResponse(RegistryObjectType.CONTACT, public_request_id, form.log_entry_type,
                                    form.cleaned_data['handle'], custom_email, form.cleaned_data['confirmation_method'])

    def get_success_url(self) -> str:
        if self.success_url:
            return force_str(self.success_url)
        if self.form_cleaned_data['send_to'].choice == 'email_in_registry':
            url_name = 'webwhois:email_in_registry_response'
        else:
            assert self.form_cleaned_data['send_to'].choice == 'custom_email'  # noqa: S101
            url_name = 'webwhois:public_response'
        return reverse(url_name, kwargs={'public_key': self.public_key},
                       current_app=cast(ResolverMatch, self.request.resolver_match).namespace)


class BlockUnblockFormView(PublicRequestFormView[FormType]):
    """Block or Unblock object form view."""

    # XXX: Ignores are not great, but it doesn't change behaviour.
    form_class: Type[Form] = None  # type: ignore[assignment]
    block_action: str = None  # type: ignore[assignment]
    logging_lock_type: Dict[str, str] = None  # type: ignore[assignment]
    form_cleaned_data: Dict[str, Any] = None  # type: ignore[assignment]

    def _get_lock_type(self, key: str) -> EnumItem:
        raise NotImplementedError

    def _call_registry_command(self, form: FormType, log_request_id: int) -> int:
        try:
            response_id = PUBLIC_REQUEST.create_block_unblock_request(
                self._get_object_type(form.cleaned_data['object_type']), form.cleaned_data['handle'], log_request_id,
                CONFIRMATION_METHOD_IDL_MAP[form.cleaned_data['confirmation_method']],
                self._get_lock_type(form.cleaned_data['lock_type']))
        except OBJECT_NOT_FOUND as err:
            form.add_error('handle', _('Object not found. Check that you have correctly entered the Object type and '
                                       'Handle.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except OBJECT_ALREADY_BLOCKED as err:
            form.add_error('handle', _('This object is already blocked. The request can not be accepted.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except OBJECT_NOT_BLOCKED as err:
            form.add_error('handle', _('This object is not blocked. The request can not be accepted.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except HAS_DIFFERENT_BLOCK as err:
            form.add_error('handle', _('This object has another active blocking. The request can not be accepted.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        except OPERATION_PROHIBITED as err:
            form.add_error('handle', _('Operation for this object is prohibited. The request can not be accepted.'))
            raise PublicRequestKnownException(type(err).__name__) from err
        return cast(int, response_id)

    def get_public_response(self, form: FormType, public_request_id: int) -> BlockResponse:
        return BlockResponse(form.cleaned_data['object_type'], public_request_id, form.log_entry_type,
                             form.cleaned_data['handle'], self.block_action, form.cleaned_data['lock_type'],
                             form.cleaned_data['confirmation_method'])

    def get_initial(self) -> Dict[str, Any]:
        data = super().get_initial()
        data["handle"] = self.request.GET.get("handle")
        data["object_type"] = self.request.GET.get("object_type")
        lock_type = self.request.GET.get(LOCK_TYPE_URL_PARAM)
        if lock_type and lock_type in (LOCK_TYPE_TRANSFER, LOCK_TYPE_ALL):
            data["lock_type"] = lock_type
        return data

    def get_success_url(self) -> str:
        if self.success_url:
            return force_str(self.success_url)
        return reverse('webwhois:public_response', kwargs={'public_key': self.public_key},
                       current_app=cast(ResolverMatch, self.request.resolver_match).namespace)


class BlockObjectFormView(BlockUnblockFormView[BlockObjectForm]):
    """Block object form view."""

    form_class = BlockObjectForm
    template_name = 'webwhois/form_block_object.html'
    block_action = 'block'
    logging_lock_type = {
        LOCK_TYPE_TRANSFER: PublicRequestsLogEntryType.BLOCK_TRANSFER,
        LOCK_TYPE_ALL: PublicRequestsLogEntryType.BLOCK_CHANGES,
    }

    def _get_lock_type(self, key: str) -> EnumItem:
        return {
            LOCK_TYPE_TRANSFER: LockRequestType.block_transfer,
            LOCK_TYPE_ALL: LockRequestType.block_transfer_and_update,
        }[key]


class UnblockObjectFormView(BlockUnblockFormView):
    """Unblock object form view."""

    form_class = UnblockObjectForm
    template_name = 'webwhois/form_unblock_object.html'
    block_action = 'unblock'
    logging_lock_type = {
        LOCK_TYPE_TRANSFER: PublicRequestsLogEntryType.UNBLOCK_TRANSFER,
        LOCK_TYPE_ALL: PublicRequestsLogEntryType.UNBLOCK_CHANGES,
    }

    def _get_lock_type(self, key: str) -> EnumItem:
        return {
            LOCK_TYPE_TRANSFER: LockRequestType.unblock_transfer,
            LOCK_TYPE_ALL: LockRequestType.unblock_transfer_and_update,
        }[key]


class PublicResponseNotFound(Exception):
    """Public response was not found in the cache."""


class PublicResponseNotFoundView(TemplateView):
    """Response Not found view."""

    template_name = 'webwhois/public_request_response_not_found.html'


class PublicResponseMixin:
    """Mixin for public response views."""

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self._public_response: Optional[PublicResponse] = None

    def get_public_response(self) -> PublicResponse:
        """Return relevant public response."""
        # Cache the result for case the cache gets deleted while handling the request.
        if self._public_response is None:
            public_key = cast(View, self).kwargs['public_key']
            public_response = cache.get(public_key)
            if public_response is None:
                raise PublicResponseNotFound(public_key)
            self._public_response = public_response
        return cast(PublicResponse, self._public_response)


class BaseResponseTemplateView(PublicResponseMixin, TemplateView):
    """Base view for public request responses."""

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        try:
            self.get_public_response()
        except PublicResponseNotFound:
            return HttpResponseRedirect(reverse("webwhois:response_not_found",
                                                kwargs={"public_key": kwargs['public_key']},
                                                current_app=cast(ResolverMatch, self.request.resolver_match).namespace))
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        """Add public response object to the context."""
        context = super().get_context_data(**kwargs)
        context.setdefault('public_response', self.get_public_response())
        return context


class EmailInRegistryView(BaseResponseTemplateView):
    """Email in registy view."""

    template_name = 'webwhois/public_request_email_in_registry.html'

    text_title = {
        RegistryObjectType.CONTACT: _('Request to send a password (authinfo) for transfer contact %(handle)s'),
        RegistryObjectType.DOMAIN: _('Request to send a password (authinfo) for transfer domain name %(handle)s'),
        RegistryObjectType.NSSET: _('Request to send a password (authinfo) for transfer nameserver set %(handle)s'),
        RegistryObjectType.KEYSET: _('Request to send a password (authinfo) for transfer keyset %(handle)s'),
    }
    text_content = {
        RegistryObjectType.CONTACT: _('We received successfully your request for a password to change the contact '
                                      '<strong>{handle}</strong> sponsoring registrar. An email with the password will '
                                      'be sent to the email address from the registry.'),
        RegistryObjectType.DOMAIN: _('We received successfully your request for a password to change the domain '
                                     '<strong>{handle}</strong> sponsoring registrar. An email with the password will '
                                     'be sent to the email address of domain holder and admin contacts.'),
        RegistryObjectType.NSSET: _('We received successfully your request for a password to change the nameserver set '
                                    '<strong>{handle}</strong> sponsoring registrar. An email with the password will '
                                    'be sent to the email addresses of the nameserver set\'s technical contacts.'),
        RegistryObjectType.KEYSET: _('We received successfully your request for a password to change the keyset '
                                     '<strong>{handle}</strong> sponsoring registrar. An email with the password will '
                                     'be sent to the email addresses of the keyset\'s technical contacts.'),
    }
    personal_info_title = _('Request for personal data of contact %(handle)s')
    personal_info_content = _(
        'We received your request for personal data of the contact <strong>{handle}</strong> successfully. An email '
        'with the personal data will be sent to the email address from the registry.')

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        context = super().get_context_data(**kwargs)
        public_response = self.get_public_response()
        if isinstance(public_response, SendPasswordResponse):
            title = self.text_title[public_response.object_type] % {'handle': public_response.handle}
            context['text_title'] = context['text_header'] = title
            context['text_content'] = format_html(self.text_content[public_response.object_type],
                                                  handle=public_response.handle)
        else:
            assert isinstance(public_response, PersonalInfoResponse)  # noqa: S101
            title = self.personal_info_title % {'handle': public_response.handle}
            context['text_title'] = context['text_header'] = title
            context['text_content'] = format_html(self.personal_info_content, handle=public_response.handle)
        return context


class PublicResponseView(BaseResponseTemplateView):
    """Return a page with public response."""

    template_name = 'webwhois/public_response.html'


class PublicResponsePdfView(PublicResponseMixin, View):
    """Return a PDF for the public response."""

    template_names: Dict[PublicRequestsLogEntryType, str] = {
        PublicRequestsLogEntryType.AUTH_INFO: 'public-request-auth-info-{language}.html',
        PublicRequestsLogEntryType.BLOCK_TRANSFER: 'public-request-block-{language}.html',
        PublicRequestsLogEntryType.BLOCK_CHANGES: 'public-request-block-{language}.html',
        PublicRequestsLogEntryType.UNBLOCK_TRANSFER: 'public-request-unblock-{language}.html',
        PublicRequestsLogEntryType.UNBLOCK_CHANGES: 'public-request-unblock-{language}.html',
        PublicRequestsLogEntryType.PERSONAL_INFO: 'public-request-personal-info-{language}.html',
    }

    def get(self, request: HttpRequest, public_key: str) -> HttpResponse:
        try:
            public_response = self.get_public_response()
        except PublicResponseNotFound as error:
            raise Http404 from error

        template_name = self.template_names[public_response.request_type].format(language=get_language())
        context = {
            'type': public_response.object_type,
            'identifier': public_response.public_request_id,
            'handle': public_response.handle,
            'date': public_response.create_date.isoformat(),
            'email': getattr(public_response, 'custom_email', None),
            'block_type': getattr(public_response, 'lock_type', None),
        }
        response = HttpResponse(SECRETARY_CLIENT.render_pdf(template_name, context), content_type='application/pdf')
        response['Content-Disposition'] = 'attachment; filename="public-request-{}.pdf"'.format(public_response.handle)
        return response
