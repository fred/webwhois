#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from abc import ABC, abstractmethod
from functools import lru_cache
from typing import Any, Dict, Generic, TypeVar, Union, cast

from django.views.generic.base import TemplateView, View
from fred_types import RegistryObjectType
from grill import References

from ..constants import LogEntryType, LogResult
from ..exceptions import WebwhoisError
from ..settings import LOGGER

ObjectType = TypeVar('ObjectType')


class RegistryObjectView(TemplateView, Generic[ObjectType], ABC):
    """Base class for loading object registry of the handle."""

    log_entry_type = LogEntryType.INFO

    @property
    @abstractmethod
    def object_type(self) -> Union[RegistryObjectType, str]:
        """Return name of the object type this view handles."""

    def load_related_objects(self, context: Dict[str, Any]) -> None:
        """Load objects related to the main registry object and append them into the context."""

    def _make_context(self, obj: ObjectType) -> Dict[str, Any]:
        """Turn object into a context."""
        return {self.object_type: {"detail": obj}}

    @lru_cache  # noqa: B019
    def get_object(self) -> ObjectType:
        """Fetch and return an object from registry.

        Raises:
            WebwhoisError: If the object is not found in registry backend.
        """
        handle = cast(View, self).kwargs["handle"]
        properties = {"handle": handle, "handleType": self.object_type}
        with LOGGER.create(self.log_entry_type, source_ip=cast(View, self).request.META.get('REMOTE_ADDR', ''),
                           properties=properties) as log_entry:
            try:
                obj = self._get_object(handle)
            except WebwhoisError:
                log_entry.result = LogResult.NOT_FOUND
                raise
            except BaseException as error:
                log_entry.result = LogResult.ERROR
                log_entry.properties["exception"] = error.__class__.__name__
                raise
            else:
                log_entry.references = self._get_references(obj)
                log_entry.result = LogResult.SUCCESS
            return obj

    @abstractmethod
    def _get_object(self, handle: str) -> ObjectType:
        """Actually fetches and returns the object from registry.

        Arguments:
            handle: Handle of an object to find.

        Raises:
            WebwhoisError: If the object is not found in registry backend.
        """

    @abstractmethod
    def _get_references(self, obj: ObjectType) -> References:
        """Return log references for the object."""

    def get_context_data(self, handle: str, **kwargs: Any) -> Dict[str, Any]:  # type: ignore[override]
        kwargs.setdefault("handle", handle)
        obj = self.get_object()
        context = {}
        context.update(self._make_context(obj))
        self.load_related_objects(context)
        context.update(kwargs)
        return super().get_context_data(**context)
