#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from typing import Any, Dict, Optional, cast

from django.urls import reverse
from django.utils.translation import gettext as _
from fred_types import RegistryObjectType
from grill import References
from regal import Domain, DomainLifeCycleStage, Keyset, Nsset
from regal.domain import DomainLifeCycleStageResult

from webwhois.utils.cdnskey_client import get_cdnskey_client

from ..constants import REGISTRAR
from ..exceptions import WebwhoisError
from ..settings import DOMAIN_CLIENT, KEYSET_CLIENT, NSSET_CLIENT, REGISTRAR_CLIENT
from .base import RegistryObjectView
from .detail_keyset import get_keyset_context
from .detail_nsset import get_nsset_context


class DomainDetailView(RegistryObjectView[Optional[Domain]]):

    template_name = "webwhois/domain.html"
    object_type = RegistryObjectType.DOMAIN

    def _get_object(self, handle: str) -> Optional[Domain]:
        """Return the domain object, or None if it's a delete candidate."""
        result = cast(DomainLifeCycleStageResult, DOMAIN_CLIENT.get_life_cycle_stage(handle))
        if result.stage == DomainLifeCycleStage.REGISTERED:
            return cast(Domain, DOMAIN_CLIENT.get_domain_info(result.id))
        elif result.stage == DomainLifeCycleStage.DELETE_CANDIDATE:
            return None
        elif result.stage == DomainLifeCycleStage.IN_AUCTION:
            raise WebwhoisError(handle, 'IN_AUCTION', message=_("Domain in auction"))
        elif result.stage == DomainLifeCycleStage.BLACKLISTED:
            raise WebwhoisError(handle, 'BLACKLISTED', message=_("Domain on blacklist"))
        else:
            raise WebwhoisError(handle, 'OBJECT_NOT_FOUND', message=_("Domain not found"))

    def _get_references(self, obj: Optional[Domain]) -> References:
        """Return log references for the object."""
        if obj:
            return {self.object_type: obj.domain_id}
        else:
            return {}

    def _make_context(self, obj: Optional[Domain]) -> Dict[str, Any]:
        """Turn object into a context."""
        if obj is None:
            # Make a domain-like context for delete candidate.
            detail = {"fqdn": self.kwargs['handle']}
            return {self.object_type: {"detail": detail, 'flags': ['deleteCandidate']}}
        else:
            return super()._make_context(obj)

    def load_related_objects(self, context: Dict[str, Any]) -> None:
        """Load objects related to the domain and append them into the context."""
        obj = self.get_object()
        if obj is None:
            # Domain is a delete candidate, don't do anything.
            return

        data = context[self.object_type]
        assert obj.registrant is not None  # noqa: S101
        data.update({
            REGISTRAR: REGISTRAR_CLIENT.get_registrar_info(obj.sponsoring_registrar),
            "flags": [k for k, v in DOMAIN_CLIENT.get_domain_state(obj.domain_id, internal=False).items() if v],
        })
        if obj.nsset:
            nsset = cast(Nsset, NSSET_CLIENT.get_nsset_info(obj.nsset.id))
            nsset_context = {"detail": nsset}
            nsset_context.update(get_nsset_context(nsset))
            context[RegistryObjectType.NSSET] = nsset_context
        if obj.keyset:
            keyset = cast(Keyset, KEYSET_CLIENT.get_keyset_info(obj.keyset.id))
            keyset_context = {"detail": keyset}
            keyset_context.update(get_keyset_context(keyset))
            context[RegistryObjectType.KEYSET] = keyset_context

    def get_context_data(self, handle: str, **kwargs: Any) -> Dict[str, Any]:  # type: ignore[override]
        context = super().get_context_data(handle=handle, **kwargs)
        if get_cdnskey_client() is not None:
            context['scan_results_link'] = reverse('webwhois:scan_results', kwargs={'handle': handle})
        return context
