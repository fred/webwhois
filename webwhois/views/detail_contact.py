#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from typing import Any, Dict, cast

from django.utils.translation import gettext as _
from fred_types import RegistryObjectType
from grill import References
from regal import Contact
from regal.exceptions import ContactDoesNotExist

from ..exceptions import WebwhoisError
from ..settings import CONTACT_CLIENT, REGISTRAR_CLIENT
from .base import RegistryObjectView


class ContactDetailView(RegistryObjectView[Contact]):

    template_name = "webwhois/contact.html"
    object_type = RegistryObjectType.CONTACT

    def _get_object(self, handle: str) -> Contact:
        try:
            return cast(Contact, CONTACT_CLIENT.get_contact_info(CONTACT_CLIENT.get_contact_id(handle)))
        except ContactDoesNotExist as error:
            raise WebwhoisError(handle, 'OBJECT_NOT_FOUND', message=_("Contact not found")) from error

    def _get_references(self, obj: Contact) -> References:
        """Return log references for the object."""
        return {self.object_type: obj.contact_id}

    def load_related_objects(self, context: Dict[str, Any]) -> None:
        """Load objects related to the contact and append them into the context."""
        obj = self.get_object()
        data = context[self.object_type]
        data['flags'] = [k for k, v in CONTACT_CLIENT.get_contact_state(obj.contact_id, internal=False).items() if v]
        assert obj.events  # noqa: S101
        data["creating_registrar"] = REGISTRAR_CLIENT.get_registrar_info(obj.events.registered.registrar_handle)
        data["sponsoring_registrar"] = REGISTRAR_CLIENT.get_registrar_info(obj.sponsoring_registrar)
