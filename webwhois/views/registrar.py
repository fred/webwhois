#
# Copyright (C) 2015-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
import random
from contextlib import suppress
from datetime import datetime
from functools import cached_property
from typing import Any, Dict, List, Optional, cast

from django.http import Http404, HttpRequest, StreamingHttpResponse
from django.utils import timezone
from django.utils.translation import gettext as _
from django.views.generic import TemplateView, View
from grill import References
from regal import Registrar, RegistrarCertification
from regal.exceptions import RegistrarDoesNotExist

from ..constants import REGISTRAR
from ..exceptions import WebwhoisError
from ..settings import REGISTRAR_CLIENT, STORAGE
from .base import RegistryObjectView


class RegistrarDetailView(RegistryObjectView[Registrar]):
    """Detail of Registrar."""

    template_name = "webwhois/registrar.html"
    object_type = REGISTRAR

    def _get_object(self, handle: str) -> Registrar:
        try:
            return cast(Registrar, REGISTRAR_CLIENT.get_registrar_info(handle))
        except RegistrarDoesNotExist as error:
            raise WebwhoisError(handle, 'OBJECT_NOT_FOUND', message=_("Registrar not found")) from error

    def _get_references(self, obj: Registrar) -> References:
        """Return log references for the object."""
        return {self.object_type: obj.registrar_id}


def _get_aware_now() -> datetime:
    """Return aware 'now' datetime regardless of Django settings."""
    now = timezone.now()
    if timezone.is_naive(now):
        now = timezone.make_aware(now)
    return now


class RegistrarListView(TemplateView):
    """Mixin for a list of registrars.

    @cvar group_name: Name of the registrar group to filter results.
    """

    template_name = "webwhois/registrar_list.html"
    group_name: Optional[str] = None

    def __init__(self, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        # Caches for backend responses
        self._groups: Optional[Dict[str, List[str]]] = None

    @cached_property
    def all_registrar_handles(self) -> List[str]:
        """Return a list of all registrar handles."""
        return cast(List[str], REGISTRAR_CLIENT.get_registrar_handles())

    def get_registrars(self) -> List[Registrar]:
        """Return a list of registrars to be displayed.

        Results are filtered according to `group_name` attribute.
        """
        if self.group_name:
            groups = self.get_groups()
            if self.group_name not in groups:
                raise Http404('Registrar group {} not found.'.format(self.group_name))
            members = groups[self.group_name]
        else:
            members = self.all_registrar_handles
        return [REGISTRAR_CLIENT.get_registrar_info(handle) for handle in members]

    def get_groups(self) -> Dict[str, List[str]]:
        """Return dictionary of registrar groups."""
        if self._groups is None:
            groups: Dict[str, List[str]] = {g: [] for g in REGISTRAR_CLIENT.get_registrar_groups()}
            for handle in self.all_registrar_handles:
                for group in REGISTRAR_CLIENT.get_registrar_groups_membership(handle):
                    groups[group].append(handle)
            self._groups = groups
        return self._groups

    def get_certification(self, handle: str) -> Optional[RegistrarCertification]:
        """Return dictionary of active registrar certifications."""
        now = _get_aware_now()
        for certification in cast(List[RegistrarCertification], REGISTRAR_CLIENT.get_registrar_certifications(handle)):
            if certification.valid_from <= now and (certification.valid_to is None or certification.valid_to > now):
                return certification
        return None

    def get_registrar_context(self, registrar: Registrar) -> Dict[str, Any]:
        """Return context for a registrar."""
        certification = self.get_certification(registrar.registrar_handle)
        score = certification.classification if certification else 0
        context = {REGISTRAR: registrar, "cert": certification, "score": score}
        return context

    def sort_registrars(self, registrars: List[Dict[str, Any]]) -> List[Dict[str, Any]]:
        """Sort context data of registrars.

        Registrars are randomized, but sorted according to their certification score.
        """
        # Randomize order of the list of registrars and than sort it by score.
        rand = random.SystemRandom()
        rand.shuffle(registrars)
        return sorted(registrars, key=lambda row: cast(int, row["score"]), reverse=True)

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        registrars = []
        for reg in self.get_registrars():
            registrars.append(self.get_registrar_context(reg))

        kwargs.setdefault("groups", self.get_groups())
        kwargs.setdefault("registrars", self.sort_registrars(registrars))
        return super().get_context_data(**kwargs)


class DownloadEvalFileView(View):
    def get(self, request: HttpRequest, handle: str) -> StreamingHttpResponse:
        certification = self.get_certification(handle)
        if certification:
            try:
                with STORAGE.open(certification.file_id) as file:
                    response = StreamingHttpResponse(file, content_type=file.mimetype)
                    response["Content-Disposition"] = 'inline; filename="{}"'.format(file.name)
                    return response
            except FileNotFoundError as error:
                raise Http404("File not found") from error
        raise Http404("Certification not found")

    def get_certification(self, handle: str) -> Optional[RegistrarCertification]:
        """Return registrar certification."""
        now = _get_aware_now()
        with suppress(RegistrarDoesNotExist):
            for certification in cast(List[RegistrarCertification],
                                      REGISTRAR_CLIENT.get_registrar_certifications(handle)):
                if certification.valid_from <= now and (certification.valid_to is None or certification.valid_to > now):
                    return certification
        return None
