#
# Copyright (C) 2015-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from typing import Any, Dict, cast

from django.utils.translation import gettext as _
from fred_types import RegistryObjectType
from grill import References
from regal import Keyset
from regal.exceptions import KeysetDoesNotExist

from ..constants import REGISTRAR
from ..exceptions import WebwhoisError
from ..settings import KEYSET_CLIENT, REGISTRAR_CLIENT
from .base import RegistryObjectView


def get_keyset_context(keyset: Keyset) -> Dict[str, Any]:
    return {
        "flags": [k for k, v in KEYSET_CLIENT.get_keyset_state(keyset.keyset_id, internal=False).items() if v],
        REGISTRAR: REGISTRAR_CLIENT.get_registrar_info(keyset.sponsoring_registrar),
    }


class KeysetDetailView(RegistryObjectView[Keyset]):

    template_name = "webwhois/keyset.html"
    object_type = RegistryObjectType.KEYSET

    def _get_object(self, handle: str) -> Keyset:
        try:
            return cast(Keyset, KEYSET_CLIENT.get_keyset_info(KEYSET_CLIENT.get_keyset_id(handle)))
        except KeysetDoesNotExist as error:
            raise WebwhoisError(handle, 'OBJECT_NOT_FOUND', message=_("Key set not found")) from error

    def _get_references(self, obj: Keyset) -> References:
        """Return log references for the object."""
        return {self.object_type: obj.keyset_id}

    def load_related_objects(self, context: Dict[str, Any]) -> None:
        """Load objects related to the keyset and append them into the context."""
        obj = self.get_object()
        data = context[self.object_type]
        data.update(get_keyset_context(obj))
