#!/usr/bin/python
#
# Copyright (C) 2015-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from datetime import datetime, timezone
from http import HTTPStatus
from typing import List
from unittest.mock import MagicMock, call, patch

from django.http import HttpResponse
from django.test import SimpleTestCase
from django.test.utils import override_settings
from django.urls import reverse
from filed import File
from fred_types import RegistrarId
from freezegun import freeze_time
from grill.utils import TestLogEntry, TestLoggerClient
from regal import FileId, Registrar, RegistrarCertification, RegistrarCertificationId
from regal.exceptions import RegistrarDoesNotExist

from webwhois.constants import LOGGER_SERVICE, REGISTRAR, LogEntryType, LogResult
from webwhois.tests.utils import TEMPLATES


@override_settings(ROOT_URLCONF='webwhois.tests.urls', STATIC_URL='/static/', TEMPLATES=TEMPLATES)
class RegistrarDetailViewTest(SimpleTestCase):
    object_id = RegistrarId("HOLLY")

    def setUp(self):
        patcher = patch('webwhois.views.registrar.REGISTRAR_CLIENT')
        self.addCleanup(patcher.stop)
        self.registrar_mock = patcher.start()

        self.test_logger = TestLoggerClient()
        log_patcher = patch('webwhois.settings.LOGGER.client', new=self.test_logger)
        self.addCleanup(log_patcher.stop)
        log_patcher.start()

    def test_registrar_not_found(self):
        self.registrar_mock.get_registrar_info.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("webwhois:detail_registrar", kwargs={"handle": "REG_FRED_A"}))

        self.assertContains(response, 'Registrar not found', status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('REG_FRED_A')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': 'REG_FRED_A', 'handleType': REGISTRAR})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_registrar(self):
        self.registrar_mock.get_registrar_info.return_value = Registrar(
            registrar_id=self.object_id, registrar_handle="REG_FRED_A", name='Holly')

        response = self.client.get(reverse("webwhois:detail_registrar", kwargs={"handle": "REG_FRED_A"}))

        self.assertContains(response, "Registrar details")
        self.assertContains(response, "REG_FRED_A")
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('REG_FRED_A')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'REG_FRED_A', 'handleType': REGISTRAR},
                                 references={REGISTRAR: self.object_id})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())


@override_settings(ROOT_URLCONF='webwhois.tests.urls', TEMPLATES=TEMPLATES)
class RegistrarListViewTest(SimpleTestCase):
    def setUp(self):
        patcher = patch('webwhois.views.registrar.REGISTRAR_CLIENT')
        self.addCleanup(patcher.stop)
        self.registrar_mock = patcher.start()
        self.registrar_mock.get_registrar_handles.return_value = []
        self.registrar_mock.get_registrar_groups.return_value = []
        self.registrar_mock.get_registrar_certifications.return_value = []
        self.registrar_mock.get_registrar_groups_membership.return_value = []

    def test_registrars_empty(self):
        response = self.client.get(reverse('webwhois:registrars'))

        self.assertContains(response, "List of registrars")
        self.assertEqual(len(response.context['registrars']), 0)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_handles(), call.get_registrar_groups()])

    def test_registrars(self):
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY"]
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar

        response = self.client.get(reverse('webwhois:registrars'))

        self.assertContains(response, "List of registrars")
        self.assertEqual(len(response.context['registrars']), 1)
        self.assertEqual(response.context['registrars'][0]['registrar'], registrar)
        calls = [call.get_registrar_handles(), call.get_registrar_info("HOLLY"),
                 call.get_registrar_certifications('HOLLY'), call.get_registrar_groups(),
                 call.get_registrar_groups_membership('HOLLY')]
        self.assertEqual(self.registrar_mock.mock_calls, calls)

    def _test_registrars_certifications(self):
        # Test registrar with certification.
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY"]
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar
        certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), classification=3, file_id=FileId('THE-END'))
        self.registrar_mock.get_registrar_certifications.return_value = [certification]

        response = self.client.get(reverse('webwhois:registrars'))

        self.assertContains(response, "List of registrars")
        self.assertEqual(len(response.context['registrars']), 1)
        self.assertEqual(response.context['registrars'][0]['cert'], certification)
        calls = [call.get_registrar_handles(), call.get_registrar_info("HOLLY"),
                 call.get_registrar_certifications('HOLLY'), call.get_registrar_groups(),
                 call.get_registrar_groups_membership('HOLLY')]
        self.assertEqual(self.registrar_mock.mock_calls, calls)

    def test_registrars_certifications_use_tz(self):
        with override_settings(USE_TZ=True):
            self._test_registrars_certifications()

    def test_registrars_certifications_no_use_tz(self):
        with override_settings(USE_TZ=False):
            self._test_registrars_certifications()

    def test_registrars_group(self):
        # Test registrars filtered by group.
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY"]
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar
        self.registrar_mock.get_registrar_groups.return_value = ['red_dwarf']
        self.registrar_mock.get_registrar_groups_membership.return_value = ['red_dwarf']

        response = self.client.get(reverse('registrars_red_dwarf'))

        self.assertContains(response, "List of registrars")
        self.assertEqual(len(response.context['registrars']), 1)
        self.assertEqual(response.context['registrars'][0]['registrar'], registrar)
        calls = [call.get_registrar_groups(), call.get_registrar_handles(),
                 call.get_registrar_groups_membership('HOLLY'), call.get_registrar_info("HOLLY"),
                 call.get_registrar_certifications('HOLLY')]
        self.assertEqual(self.registrar_mock.mock_calls, calls)

    def test_registrars_group_mismatch(self):
        # Test registrars filtered by group - not a member, or the group does not exist.
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY"]
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar
        self.registrar_mock.get_registrar_groups_membership.return_value = []

        response = self.client.get(reverse('registrars_red_dwarf'))

        self.assertContains(response, "Registrar group red_dwarf not found", status_code=404)
        calls = [call.get_registrar_groups(), call.get_registrar_handles(),
                 call.get_registrar_groups_membership('HOLLY')]
        self.assertEqual(self.registrar_mock.mock_calls, calls)

    def _test_registrar_list(self, registrars: List[Registrar]) -> HttpResponse:
        # Run the registrar list with random shuffle replaced with sorting by name.
        def _sort_by_name(registrars):
            registrars.sort(key=lambda row: row['registrar'].registrar_handle)

        with patch("webwhois.views.registrar.random.SystemRandom.shuffle", side_effect=_sort_by_name):
            response = self.client.get(reverse('webwhois:registrars'))

        self.assertContains(response, "List of registrars")
        self.assertEqual([r['registrar'] for r in response.context['registrars']], registrars)
        return response

    def test_registrars_shuffle(self):
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY", "QUEEG", "GORDON"]
        holly = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        queeg = Registrar(registrar_id=RegistrarId("QUEEG"), registrar_handle="QUEEG", name='Queeg')
        gordon = Registrar(registrar_id=RegistrarId("GORDON"), registrar_handle="GORDON", name='Gordon')
        self.registrar_mock.get_registrar_info.side_effect = [holly, queeg, gordon]

        self._test_registrar_list([gordon, holly, queeg])

        calls = [
            call.get_registrar_handles(), call.get_registrar_info("HOLLY"), call.get_registrar_info("QUEEG"),
            call.get_registrar_info("GORDON"), call.get_registrar_certifications('HOLLY'),
            call.get_registrar_certifications('QUEEG'), call.get_registrar_certifications('GORDON'),
            call.get_registrar_groups(), call.get_registrar_groups_membership('HOLLY'),
            call.get_registrar_groups_membership('QUEEG'), call.get_registrar_groups_membership('GORDON')]
        self.assertEqual(self.registrar_mock.mock_calls, calls)

    def test_registrars_sorting(self):
        # Test registrars are sorted by the certification value.
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY", "QUEEG", "GORDON"]
        holly = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        holly_certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), classification=1, file_id=FileId('THE-END'))
        queeg = Registrar(registrar_id=RegistrarId("QUEEG"), registrar_handle="QUEEG", name='Queeg')
        queeg_certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='QUEEG',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), classification=2, file_id=FileId('THE-END'))
        gordon = Registrar(registrar_id=RegistrarId("GORDON"), registrar_handle="GORDON", name='Gordon')
        gordon_certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='GORDON',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), classification=5, file_id=FileId('THE-END'))
        self.registrar_mock.get_registrar_info.side_effect = [holly, queeg, gordon]
        self.registrar_mock.get_registrar_certifications.side_effect = [
            [holly_certification], [queeg_certification], [gordon_certification]]

        self._test_registrar_list([gordon, queeg, holly])

        calls = [
            call.get_registrar_handles(), call.get_registrar_info("HOLLY"), call.get_registrar_info("QUEEG"),
            call.get_registrar_info("GORDON"), call.get_registrar_certifications('HOLLY'),
            call.get_registrar_certifications('QUEEG'), call.get_registrar_certifications('GORDON'),
            call.get_registrar_groups(), call.get_registrar_groups_membership('HOLLY'),
            call.get_registrar_groups_membership('QUEEG'), call.get_registrar_groups_membership('GORDON')]
        self.assertEqual(self.registrar_mock.mock_calls, calls)

    def test_registrars_sorting_ignore_old(self):
        # Test old certifications are ignored when sorting registrars.
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY", "QUEEG", "GORDON"]
        holly = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        holly_certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), valid_to=datetime(1989, 2, 15, tzinfo=timezone.utc),
            classification=1, file_id=FileId('THE-END'))
        queeg = Registrar(registrar_id=RegistrarId("QUEEG"), registrar_handle="QUEEG", name='Queeg')
        gordon = Registrar(registrar_id=RegistrarId("GORDON"), registrar_handle="GORDON", name='Gordon')
        self.registrar_mock.get_registrar_info.side_effect = [holly, queeg, gordon]
        self.registrar_mock.get_registrar_certifications.side_effect = [[holly_certification], [], []]

        with freeze_time('1990-01-01'):
            self._test_registrar_list([gordon, holly, queeg])

    def test_registrars_sorting_ignore_inactive(self):
        # Test certifications not yet active are ignored when sorting registrars.
        self.registrar_mock.get_registrar_handles.return_value = ["HOLLY", "QUEEG", "GORDON"]
        holly = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        holly_certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), valid_to=datetime(1989, 2, 15, tzinfo=timezone.utc),
            classification=1, file_id=FileId('THE-END'))
        queeg = Registrar(registrar_id=RegistrarId("QUEEG"), registrar_handle="QUEEG", name='Queeg')
        gordon = Registrar(registrar_id=RegistrarId("GORDON"), registrar_handle="GORDON", name='Gordon')
        self.registrar_mock.get_registrar_info.side_effect = [holly, queeg, gordon]
        self.registrar_mock.get_registrar_certifications.side_effect = [[holly_certification], [], []]

        with freeze_time('1987-01-01'):
            self._test_registrar_list([gordon, holly, queeg])


@override_settings(TEMPLATES=TEMPLATES, ROOT_URLCONF='webwhois.tests.urls')
class DownloadEvalFileViewTest(SimpleTestCase):
    def setUp(self):
        patcher = patch('webwhois.views.registrar.REGISTRAR_CLIENT')
        self.addCleanup(patcher.stop)
        self.registrar_mock = patcher.start()
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar

        patcher = patch('webwhois.views.registrar.STORAGE', autospec=True)
        self.addCleanup(patcher.stop)
        self.storage_mock = patcher.start()

    def test_get_registrar_does_not_exist(self):
        self.registrar_mock.get_registrar_certifications.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("webwhois:download_evaluation_file", kwargs={"handle": "HOLLY"}))

        self.assertContains(response, 'Certification not found', status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_certifications('HOLLY')])
        self.assertEqual(self.storage_mock.mock_calls, [])

    def test_get_certification_does_not_exist(self):
        self.registrar_mock.get_registrar_certifications.return_value = []

        response = self.client.get(reverse("webwhois:download_evaluation_file", kwargs={"handle": "HOLLY"}))

        self.assertContains(response, 'Certification not found', status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_certifications('HOLLY')])
        self.assertEqual(self.storage_mock.mock_calls, [])

    def test_get_certification_inactive(self):
        certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), valid_to=datetime(1989, 2, 15, tzinfo=timezone.utc),
            classification=1, file_id=FileId('THE-END'))
        self.registrar_mock.get_registrar_certifications.return_value = [certification]

        with freeze_time('1987-01-01'):
            response = self.client.get(reverse("webwhois:download_evaluation_file", kwargs={"handle": "HOLLY"}))

        self.assertContains(response, 'Certification not found', status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_certifications('HOLLY')])
        self.assertEqual(self.storage_mock.mock_calls, [])

    def test_get_certification_too_old(self):
        certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), valid_to=datetime(1989, 2, 15, tzinfo=timezone.utc),
            classification=1, file_id=FileId('THE-END'))
        self.registrar_mock.get_registrar_certifications.return_value = [certification]

        with freeze_time('1990-01-01'):
            response = self.client.get(reverse("webwhois:download_evaluation_file", kwargs={"handle": "HOLLY"}))

        self.assertContains(response, 'Certification not found', status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_certifications('HOLLY')])
        self.assertEqual(self.storage_mock.mock_calls, [])

    def test_file_not_found(self):
        certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), classification=1, file_id=FileId('THE-END'))
        self.registrar_mock.get_registrar_certifications.return_value = [certification]
        self.storage_mock.open.side_effect = FileNotFoundError

        response = self.client.get(reverse("webwhois:download_evaluation_file", kwargs={"handle": "REG-MISSING"}))

        self.assertContains(response, 'File not found', status_code=HTTPStatus.NOT_FOUND)

    def _test_download_eval_file(self):
        content = b'Gazpacho!'
        file = MagicMock(File, mimetype='application/pdf')
        file.name = 'secret-file.txt'
        file.__iter__.return_value = iter([content])
        self.storage_mock.open.return_value.__enter__.return_value = file
        certification = RegistrarCertification(
            certification_id=RegistrarCertificationId('42'), registrar_handle='HOLLY',
            valid_from=datetime(1988, 2, 15, tzinfo=timezone.utc), classification=1, file_id=FileId('THE-END'))
        self.registrar_mock.get_registrar_certifications.return_value = [certification]

        response = self.client.get(reverse("webwhois:download_evaluation_file", kwargs={"handle": "HOLLY"}))

        self.assertEqual(response.getvalue(), content)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Disposition'], 'inline; filename="secret-file.txt"')
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_certifications('HOLLY')])
        calls = [call.open('THE-END'), call.open().__enter__(), call.open().__enter__().__iter__(),
                 call.open().__exit__(None, None, None), call.open().__enter__().close()]
        self.assertEqual(self.storage_mock.mock_calls, calls)

    def test_download_eval_file_use_tz(self):
        with override_settings(USE_TZ=True):
            self._test_download_eval_file()

    def test_download_eval_file_no_use_tz(self):
        with override_settings(USE_TZ=False):
            self._test_download_eval_file()
