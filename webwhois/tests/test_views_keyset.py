#!/usr/bin/python
#
# Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from http import HTTPStatus
from unittest.mock import call, patch

from django.test import SimpleTestCase
from django.test.utils import override_settings
from django.urls import reverse
from fred_types import KeysetId, RegistrarId, RegistryObjectType
from grill.utils import TestLogEntry, TestLoggerClient
from regal import Keyset, ObjectEvent, ObjectEvents, Registrar
from regal.exceptions import KeysetDoesNotExist

from webwhois.constants import LOGGER_SERVICE, LogEntryType, LogResult
from webwhois.views.detail_keyset import get_keyset_context

from .utils import TEMPLATES


class GetKeysetContextTest(SimpleTestCase):
    def setUp(self):
        keyset_patcher = patch('webwhois.views.detail_keyset.KEYSET_CLIENT')
        self.addCleanup(keyset_patcher.stop)
        self.keyset_mock = keyset_patcher.start()

        registrar_patcher = patch('webwhois.views.detail_keyset.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()

    def test_simple(self):
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar
        self.keyset_mock.get_keyset_state.return_value = {'positive': True, 'negative': False}
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        keyset = Keyset(keyset_id=KeysetId('KEYSET-ID'), keyset_handle='mynssid', sponsoring_registrar='HOLLY',
                        events=events)

        context = get_keyset_context(keyset)

        self.assertEqual(context, {"flags": ['positive'], "registrar": registrar})
        self.assertEqual(self.keyset_mock.mock_calls, [call.get_keyset_state('KEYSET-ID', internal=False)])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('HOLLY')])


@override_settings(ROOT_URLCONF='webwhois.tests.urls', TEMPLATES=TEMPLATES)
class DetailKeysetTest(SimpleTestCase):
    object_id = KeysetId('KEYSET-ID')

    def setUp(self):
        patcher = patch('webwhois.views.detail_keyset.KEYSET_CLIENT')
        self.addCleanup(patcher.stop)
        self.keyset_mock = patcher.start()
        self.keyset_mock.get_keyset_state.return_value = {}

        registrar_patcher = patch('webwhois.views.detail_keyset.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()
        self.registrar_mock.get_registrar_info.return_value = Registrar(
            registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')

        self.test_logger = TestLoggerClient()
        log_patcher = patch('webwhois.settings.LOGGER.client', new=self.test_logger)
        self.addCleanup(log_patcher.stop)
        log_patcher.start()

    def test_keyset_not_found(self):
        self.keyset_mock.get_keyset_id.side_effect = KeysetDoesNotExist

        response = self.client.get(reverse("webwhois:detail_keyset", kwargs={"handle": "mykeysid"}))

        self.assertContains(response, "Key set not found", status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.keyset_mock.mock_calls, [call.get_keyset_id('mykeysid')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': 'mykeysid', 'handleType': RegistryObjectType.KEYSET})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_keyset(self):
        self.keyset_mock.get_keyset_id.return_value = self.object_id
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        self.keyset_mock.get_keyset_info.return_value = Keyset(
            keyset_id=self.object_id, keyset_handle='mykeysid', sponsoring_registrar='HOLLY', events=events)

        response = self.client.get(reverse("webwhois:detail_keyset", kwargs={"handle": "mykeysid"}))

        self.assertContains(response, "Key set details")
        self.assertContains(response, "mykeysid")
        calls = [call.get_keyset_id('mykeysid'), call.get_keyset_info(self.object_id),
                 call.get_keyset_state(self.object_id, internal=False)]
        self.assertEqual(self.keyset_mock.mock_calls, calls)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('HOLLY')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'mykeysid', 'handleType': RegistryObjectType.KEYSET},
                                 references={RegistryObjectType.KEYSET: self.object_id})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())
