#!/usr/bin/python
#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from http import HTTPStatus
from typing import Any, Dict
from unittest.mock import call, patch

from django.test import SimpleTestCase
from django.test.utils import override_settings
from django.urls import reverse
from fred_types import ContactId, DomainId, KeysetId, NssetId, RegistrarId, RegistryObjectType
from grill.utils import TestLogEntry, TestLoggerClient
from regal import Contact, Domain, DomainLifeCycleStage, Keyset, Nsset, ObjectEvent, ObjectEvents, Registrar
from regal.domain import DomainLifeCycleStageResult
from regal.exceptions import ContactDoesNotExist, KeysetDoesNotExist, NssetDoesNotExist, RegistrarDoesNotExist

from webwhois.constants import LOGGER_SERVICE, REGISTRAR, LogEntryType, LogResult

from .utils import TEMPLATES


@override_settings(TEMPLATES=TEMPLATES, ROOT_URLCONF='webwhois.tests.urls')
class TestResolveHandleType(SimpleTestCase):
    def setUp(self):
        super().setUp()
        contact_patcher = patch('webwhois.views.resolve_handle_type.CONTACT_CLIENT')
        self.addCleanup(contact_patcher.stop)
        self.contact_mock = contact_patcher.start()
        domain_patcher = patch('webwhois.views.resolve_handle_type.DOMAIN_CLIENT')
        self.addCleanup(domain_patcher.stop)
        self.domain_mock = domain_patcher.start()
        keyset_patcher = patch('webwhois.views.resolve_handle_type.KEYSET_CLIENT')
        self.addCleanup(keyset_patcher.stop)
        self.keyset_mock = keyset_patcher.start()
        nsset_patcher = patch('webwhois.views.resolve_handle_type.NSSET_CLIENT')
        self.addCleanup(nsset_patcher.stop)
        self.nsset_mock = nsset_patcher.start()
        registrar_patcher = patch('webwhois.views.resolve_handle_type.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()

        self.test_logger = TestLoggerClient()
        log_patcher = patch('webwhois.settings.LOGGER.client', new=self.test_logger)
        self.addCleanup(log_patcher.stop)
        log_patcher.start()

    def test_handle_not_found(self):
        self.contact_mock.get_contact_id.side_effect = ContactDoesNotExist
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(
            DomainLifeCycleStage.NOT_REGISTERED)
        self.keyset_mock.get_keyset_id.side_effect = KeysetDoesNotExist
        self.nsset_mock.get_nsset_id.side_effect = NssetDoesNotExist
        self.registrar_mock.get_registrar_info.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("webwhois:registry_object_type", kwargs={"handle": "testhandle"}))

        self.assertContains(response, "Record not found", status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.contact_mock.mock_calls, [call.get_contact_id('testhandle')])
        self.assertEqual(self.domain_mock.mock_calls, [call.get_life_cycle_stage('testhandle')])
        self.assertEqual(self.keyset_mock.mock_calls, [call.get_keyset_id('testhandle')])
        self.assertEqual(self.nsset_mock.mock_calls, [call.get_nsset_id('testhandle')])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('testhandle')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': 'testhandle', 'handleType': 'multiple'})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_handle_with_dash_not_found(self):
        self.contact_mock.get_contact_id.side_effect = ContactDoesNotExist
        self.keyset_mock.get_keyset_id.side_effect = KeysetDoesNotExist
        self.nsset_mock.get_nsset_id.side_effect = NssetDoesNotExist
        self.registrar_mock.get_registrar_info.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("webwhois:registry_object_type", kwargs={"handle": "-abc"}))

        self.assertContains(response, "Record not found", status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.contact_mock.mock_calls, [call.get_contact_id('-abc')])
        self.assertEqual(self.keyset_mock.mock_calls, [call.get_keyset_id('-abc')])
        self.assertEqual(self.nsset_mock.mock_calls, [call.get_nsset_id('-abc')])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('-abc')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': '-abc', 'handleType': 'multiple'})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_multiple_entries(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        self.contact_mock.get_contact_id.return_value = '2X4B'
        self.contact_mock.get_contact_info.return_value = Contact(
            contact_id=ContactId('2X4B'), contact_handle='mycontact', events=events)
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(
            DomainLifeCycleStage.REGISTERED, id=DomainId('DOMAIN-ID'))
        self.domain_mock.get_domain_info.return_value = Domain(
            domain_id=DomainId('DOMAIN-ID'), fqdn='mydomain', events=events)
        self.keyset_mock.get_keyset_id.return_value = 'KEYSET-ID'
        self.keyset_mock.get_keyset_info.return_value = Keyset(
            keyset_id=KeysetId('KEYSET-ID'), keyset_handle='mykeysid', events=events)
        self.nsset_mock.get_nsset_id.return_value = 'NSSET-ID'
        self.nsset_mock.get_nsset_info.return_value = Nsset(
            nsset_id=NssetId('NSSET-ID'), nsset_handle='mynssid', events=events)
        self.registrar_mock.get_registrar_info.return_value = Registrar(
            registrar_id=RegistrarId("HOLLY-ID"), registrar_handle="HOLLY", name='Holly')

        response = self.client.get(reverse("webwhois:registry_object_type", kwargs={"handle": "testhandle.cz"}))

        self.assertContains(response, "Multiple entries found")
        self.assertEqual(self.contact_mock.mock_calls,
                         [call.get_contact_id('testhandle.cz'), call.get_contact_info('2X4B')])
        self.assertEqual(self.domain_mock.mock_calls,
                         [call.get_life_cycle_stage('testhandle.cz'), call.get_domain_info('DOMAIN-ID')])
        self.assertEqual(self.keyset_mock.mock_calls,
                         [call.get_keyset_id('testhandle.cz'), call.get_keyset_info('KEYSET-ID')])
        self.assertEqual(self.nsset_mock.mock_calls,
                         [call.get_nsset_id('testhandle.cz'), call.get_nsset_info('NSSET-ID')])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('testhandle.cz')])

        # Check logger
        references: Dict[str, Any] = {RegistryObjectType.CONTACT: '2X4B', RegistryObjectType.DOMAIN: 'DOMAIN-ID',
                                      RegistryObjectType.KEYSET: 'KEYSET-ID', RegistryObjectType.NSSET: 'NSSET-ID',
                                      REGISTRAR: 'HOLLY-ID'}
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'testhandle.cz', 'handleType': 'multiple'},
                                 references=references)
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_one_entry(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        self.contact_mock.get_contact_id.return_value = '2X4B'
        self.contact_mock.get_contact_info.return_value = Contact(
            contact_id=ContactId('2X4B'), contact_handle='mycontact', events=events)
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(
            DomainLifeCycleStage.NOT_REGISTERED)
        self.keyset_mock.get_keyset_id.side_effect = KeysetDoesNotExist
        self.nsset_mock.get_nsset_id.side_effect = NssetDoesNotExist
        self.registrar_mock.get_registrar_info.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("webwhois:registry_object_type", kwargs={"handle": "testhandle"}))

        self.assertRedirects(response, reverse("webwhois:detail_contact", kwargs={"handle": "testhandle"}),
                             fetch_redirect_response=False)
        self.assertEqual(self.contact_mock.mock_calls,
                         [call.get_contact_id('testhandle'), call.get_contact_info('2X4B')])
        self.assertEqual(self.domain_mock.mock_calls, [call.get_life_cycle_stage('testhandle')])
        self.assertEqual(self.keyset_mock.mock_calls, [call.get_keyset_id('testhandle')])
        self.assertEqual(self.nsset_mock.mock_calls, [call.get_nsset_id('testhandle')])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('testhandle')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'testhandle', 'handleType': 'multiple'},
                                 references={RegistryObjectType.CONTACT: '2X4B'})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_domain_delete_candidate(self):
        self.contact_mock.get_contact_id.side_effect = ContactDoesNotExist
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(
            DomainLifeCycleStage.DELETE_CANDIDATE)
        self.keyset_mock.get_keyset_id.side_effect = KeysetDoesNotExist
        self.nsset_mock.get_nsset_id.side_effect = NssetDoesNotExist
        self.registrar_mock.get_registrar_info.side_effect = RegistrarDoesNotExist

        response = self.client.get(reverse("webwhois:registry_object_type", kwargs={"handle": "testhandle"}))

        self.assertRedirects(response, reverse("webwhois:detail_domain", kwargs={"handle": "testhandle"}),
                             fetch_redirect_response=False)
        self.assertEqual(self.contact_mock.mock_calls,
                         [call.get_contact_id('testhandle')])
        self.assertEqual(self.domain_mock.mock_calls, [call.get_life_cycle_stage('testhandle')])
        self.assertEqual(self.keyset_mock.mock_calls, [call.get_keyset_id('testhandle')])
        self.assertEqual(self.nsset_mock.mock_calls, [call.get_nsset_id('testhandle')])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('testhandle')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'testhandle', 'handleType': 'multiple'},
                                 references={})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())
