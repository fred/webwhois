#!/usr/bin/python
#
# Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from http import HTTPStatus
from unittest.mock import call, patch

from django.test import SimpleTestCase
from django.test.utils import override_settings
from django.urls import reverse
from fred_types import NssetId, RegistrarId, RegistryObjectType
from grill.utils import TestLogEntry, TestLoggerClient
from regal import Nsset, ObjectEvent, ObjectEvents, Registrar
from regal.exceptions import NssetDoesNotExist

from webwhois.constants import LOGGER_SERVICE, LogEntryType, LogResult
from webwhois.views.detail_nsset import get_nsset_context

from .utils import TEMPLATES


class GetNssetContextTest(SimpleTestCase):
    def setUp(self):
        nsset_patcher = patch('webwhois.views.detail_nsset.NSSET_CLIENT')
        self.addCleanup(nsset_patcher.stop)
        self.nsset_mock = nsset_patcher.start()

        registrar_patcher = patch('webwhois.views.detail_nsset.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()

    def test_simple(self):
        registrar = Registrar(registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')
        self.registrar_mock.get_registrar_info.return_value = registrar
        self.nsset_mock.get_nsset_state.return_value = {'positive': True, 'negative': False}
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        nsset = Nsset(nsset_id=NssetId('NSSET-ID'), nsset_handle='mynssid', sponsoring_registrar='HOLLY', events=events)

        context = get_nsset_context(nsset)

        self.assertEqual(context, {"flags": ['positive'], "registrar": registrar})
        self.assertEqual(self.nsset_mock.mock_calls, [call.get_nsset_state('NSSET-ID', internal=False)])
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('HOLLY')])


@override_settings(ROOT_URLCONF='webwhois.tests.urls', TEMPLATES=TEMPLATES)
class DetailNssetTest(SimpleTestCase):
    object_id = NssetId('NSSET-ID')

    def setUp(self):
        patcher = patch('webwhois.views.detail_nsset.NSSET_CLIENT')
        self.addCleanup(patcher.stop)
        self.nsset_mock = patcher.start()
        self.nsset_mock.get_nsset_state.return_value = {}

        registrar_patcher = patch('webwhois.views.detail_nsset.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()
        self.registrar_mock.get_registrar_info.return_value = Registrar(
            registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')

        self.test_logger = TestLoggerClient()
        log_patcher = patch('webwhois.settings.LOGGER.client', new=self.test_logger)
        self.addCleanup(log_patcher.stop)
        log_patcher.start()

    def test_nsset_not_found(self):
        self.nsset_mock.get_nsset_id.side_effect = NssetDoesNotExist

        response = self.client.get(reverse("webwhois:detail_nsset", kwargs={"handle": "mynssid"}))

        self.assertContains(response, "Name server set not found", status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.nsset_mock.mock_calls, [call.get_nsset_id('mynssid')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': 'mynssid', 'handleType': RegistryObjectType.NSSET})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_nsset(self):
        self.nsset_mock.get_nsset_id.return_value = self.object_id
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        self.nsset_mock.get_nsset_info.return_value = Nsset(
            nsset_id=self.object_id, nsset_handle='mynssid', sponsoring_registrar='HOLLY', events=events)

        response = self.client.get(reverse("webwhois:detail_nsset", kwargs={"handle": "mynssid"}))

        self.assertContains(response, "Name server set (DNS) details")
        self.assertContains(response, "mynssid")
        calls = [call.get_nsset_id('mynssid'), call.get_nsset_info(self.object_id),
                 call.get_nsset_state(self.object_id, internal=False)]
        self.assertEqual(self.nsset_mock.mock_calls, calls)
        self.assertEqual(self.registrar_mock.mock_calls, [call.get_registrar_info('HOLLY')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'mynssid', 'handleType': RegistryObjectType.NSSET},
                                 references={RegistryObjectType.NSSET: self.object_id})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())
