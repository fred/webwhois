#
# Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from http import HTTPStatus
from typing import List, Optional
from unittest.mock import _Call, call, patch, sentinel

from django.http import HttpResponse
from django.test import SimpleTestCase
from django.test.utils import override_settings
from django.urls import reverse
from fred_types import (
    ContactId,
    ContactRef,
    DomainId,
    KeysetId,
    KeysetRef,
    NssetId,
    NssetRef,
    RegistrarId,
    RegistryObjectType,
)
from grill.utils import TestLogEntry, TestLoggerClient
from regal import Domain, DomainLifeCycleStage, Keyset, Nsset, ObjectEvent, ObjectEvents, Registrar
from regal.domain import DomainLifeCycleStageResult

from webwhois.constants import LOGGER_SERVICE, LogEntryType, LogResult

from .utils import TEMPLATES


@override_settings(ROOT_URLCONF='webwhois.tests.urls', TEMPLATES=TEMPLATES)
class DetailDomainTest(SimpleTestCase):
    object_id = DomainId('2X4B')

    def setUp(self):
        super().setUp()
        domain_patcher = patch('webwhois.views.detail_domain.DOMAIN_CLIENT')
        self.addCleanup(domain_patcher.stop)
        self.domain_mock = domain_patcher.start()
        self.domain_mock.get_domain_state.return_value = {'linked': True}

        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))

        keyset_patcher = patch('webwhois.views.detail_domain.KEYSET_CLIENT')
        self.addCleanup(keyset_patcher.stop)
        self.keyset_mock = keyset_patcher.start()
        keyset_patcher_2 = patch('webwhois.views.detail_keyset.KEYSET_CLIENT', self.keyset_mock)
        self.addCleanup(keyset_patcher_2.stop)
        keyset_patcher_2.start()
        self.keyset_mock.get_keyset_id.return_value = 'KEYSET-ID'
        self.keyset_mock.get_keyset_info.return_value = Keyset(
            keyset_id=KeysetId('KEYSET-ID'), keyset_handle='mykeysid', sponsoring_registrar='HOLLY', events=events)
        self.keyset_mock.get_keyset_state.return_value = {}

        nsset_patcher = patch('webwhois.views.detail_domain.NSSET_CLIENT')
        self.addCleanup(nsset_patcher.stop)
        self.nsset_mock = nsset_patcher.start()
        nsset_patcher_2 = patch('webwhois.views.detail_nsset.NSSET_CLIENT', self.nsset_mock)
        self.addCleanup(nsset_patcher_2.stop)
        nsset_patcher_2.start()
        self.nsset_mock.get_nsset_id.return_value = 'NSSET-ID'
        self.nsset_mock.get_nsset_info.return_value = Nsset(
            nsset_id=NssetId('NSSET-ID'), nsset_handle='mynssid', sponsoring_registrar='HOLLY', events=events)
        self.nsset_mock.get_nsset_state.return_value = {}

        registrar_patcher = patch('webwhois.views.detail_domain.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()
        registrar_patcher_2 = patch('webwhois.views.detail_keyset.REGISTRAR_CLIENT', self.registrar_mock)
        self.addCleanup(registrar_patcher_2.stop)
        registrar_patcher_2.start()
        registrar_patcher_3 = patch('webwhois.views.detail_nsset.REGISTRAR_CLIENT', self.registrar_mock)
        self.addCleanup(registrar_patcher_3.stop)
        registrar_patcher_3.start()
        self.registrar_mock.get_registrar_info.return_value = Registrar(
            registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')

        self.test_logger = TestLoggerClient()
        log_patcher = patch('webwhois.settings.LOGGER.client', new=self.test_logger)
        self.addCleanup(log_patcher.stop)
        log_patcher.start()

    def _test_domain_not_found(self, stage: DomainLifeCycleStage, message: str) -> None:
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(stage)

        response = self.client.get(reverse("webwhois:detail_domain", kwargs={"handle": "fred.cz"}))

        self.assertContains(response, message, status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.domain_mock.mock_calls, [call.get_life_cycle_stage('fred.cz')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': 'fred.cz', 'handleType': RegistryObjectType.DOMAIN})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_domain_not_found(self):
        self._test_domain_not_found(DomainLifeCycleStage.NOT_REGISTERED, "Domain not found")

    def test_domain_in_auction(self):
        self._test_domain_not_found(DomainLifeCycleStage.IN_AUCTION, "Domain in auction")

    def test_domain_blacklisted(self):
        self._test_domain_not_found(DomainLifeCycleStage.BLACKLISTED, "Domain on blacklist")

    def test_domain_delete_candidate(self):
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(
            DomainLifeCycleStage.DELETE_CANDIDATE)

        response = self.client.get(reverse("webwhois:detail_domain", kwargs={"handle": "fred.cz"}))

        self.assertContains(response, 'Domain name details')
        self.assertEqual(response.context['handle'], 'fred.cz')
        self.assertEqual(response.context['domain']['flags'], ['deleteCandidate'])
        self.assertEqual(self.domain_mock.mock_calls, [call.get_life_cycle_stage('fred.cz')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'fred.cz', 'handleType': RegistryObjectType.DOMAIN})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def _test_domain(self, domain: Domain, *, handle: str = 'fred.cz', domain_calls: Optional[List[_Call]] = None,
                     contact_calls: Optional[List[_Call]] = None,
                     nsset_calls: Optional[List[_Call]] = None, keyset_calls: Optional[List[_Call]] = None,
                     registrar_calls: Optional[List[_Call]] = None) -> HttpResponse:
        self.domain_mock.get_life_cycle_stage.return_value = DomainLifeCycleStageResult(DomainLifeCycleStage.REGISTERED,
                                                                                        self.object_id)
        self.domain_mock.get_domain_info.return_value = domain

        response = self.client.get(reverse("webwhois:detail_domain", kwargs={"handle": handle}))

        self.assertContains(response, "Domain name details")
        self.assertContains(response, handle)
        self.assertEqual(response.context['domain']['flags'], ['linked'])
        domain_calls = domain_calls or [call.get_life_cycle_stage('fred.cz'),
                                        call.get_domain_info(self.object_id),
                                        call.get_domain_state(self.object_id, internal=False)]
        self.assertEqual(self.domain_mock.mock_calls, domain_calls)
        self.assertEqual(self.keyset_mock.mock_calls, keyset_calls or [])
        self.assertEqual(self.nsset_mock.mock_calls, nsset_calls or [])
        self.assertEqual(self.registrar_mock.mock_calls, registrar_calls or [call.get_registrar_info('HOLLY')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': handle, 'handleType': RegistryObjectType.DOMAIN},
                                 references={RegistryObjectType.DOMAIN: self.object_id})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

        return response

    def test_domain(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        domain = Domain(domain_id=self.object_id, fqdn='fred.cz', events=events, sponsoring_registrar='HOLLY',
                        registrant=ContactRef(id=ContactId('CONTACT-ID'), handle='RIMMER'))
        self._test_domain(domain)

    def test_domain_nsset(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        domain = Domain(domain_id=self.object_id, fqdn='fred.cz', events=events, sponsoring_registrar='HOLLY',
                        registrant=ContactRef(id=ContactId('CONTACT-ID'), handle='RIMMER'),
                        nsset=NssetRef(id=NssetId('NSSET-ID'), handle='mynssid'))
        nsset_calls = [call.get_nsset_info('NSSET-ID'), call.get_nsset_state('NSSET-ID', internal=False)]
        registrar_calls = [call.get_registrar_info('HOLLY')] * 2
        self._test_domain(domain, nsset_calls=nsset_calls, registrar_calls=registrar_calls)

    def test_domain_keyset(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        domain = Domain(domain_id=self.object_id, fqdn='fred.cz', events=events, sponsoring_registrar='HOLLY',
                        registrant=ContactRef(id=ContactId('CONTACT-ID'), handle='RIMMER'),
                        keyset=KeysetRef(id=KeysetId('KEYSET-ID'), handle='mynssid'))
        keyset_calls = [call.get_keyset_info('KEYSET-ID'), call.get_keyset_state('KEYSET-ID', internal=False)]
        registrar_calls = [call.get_registrar_info('HOLLY')] * 2
        self._test_domain(domain, keyset_calls=keyset_calls, registrar_calls=registrar_calls)

    def test_scan_results_link(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        domain = Domain(domain_id=self.object_id, fqdn='fred.cz', events=events, sponsoring_registrar='HOLLY',
                        registrant=ContactRef(id=ContactId('CONTACT-ID'), handle='RIMMER'))

        with patch('webwhois.views.detail_domain.get_cdnskey_client', return_value=sentinel.client, autospec=True):
            response = self._test_domain(domain)

        self.assertEqual(response.context['scan_results_link'],
                         reverse('webwhois:scan_results', kwargs={'handle': 'fred.cz'}))

    def test_scan_results_link_none(self):
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        domain = Domain(domain_id=self.object_id, fqdn='fred.cz', events=events, sponsoring_registrar='HOLLY',
                        registrant=ContactRef(id=ContactId('CONTACT-ID'), handle='RIMMER'))

        with patch('webwhois.views.detail_domain.get_cdnskey_client', return_value=None, autospec=True):
            response = self._test_domain(domain)

        self.assertNotIn('scan_results_link', response.context)
