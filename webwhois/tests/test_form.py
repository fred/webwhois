#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from typing import Type, Union, cast

from django.test import SimpleTestCase, override_settings
from django.urls import reverse
from fred_types import RegistryObjectType

from webwhois.constants import PublicRequestsLogEntryType
from webwhois.forms import BlockObjectForm, SendPasswordForm, UnblockObjectForm, WhoisForm
from webwhois.forms.public_request import ConfirmationMethod, DeliveryType, PersonalInfoForm, PublicRequestBaseForm
from webwhois.tests.utils import TEMPLATES


@override_settings(TEMPLATES=TEMPLATES)
class TestWhoisForm(SimpleTestCase):

    def test_is_valid(self):
        form = WhoisForm({"handle": "foo"})
        self.assertEqual(form.errors, {})

    def test_field_is_required(self):
        form = WhoisForm({"handle": None})
        self.assertEqual(form.errors, {'handle': ['This field is required.']})
        form = WhoisForm({"handle": ""})
        self.assertEqual(form.errors, {'handle': ['This field is required.']})

    def test_max_length(self):
        form = WhoisForm({"handle": "o" * 256})
        self.assertEqual(form.errors, {'handle': ['Ensure this value has at most 255 characters (it has 256).']})

    def test_cleaned_data(self):
        form = WhoisForm({"handle": "  foo  "})
        self.assertEqual(form.errors, {})
        self.assertEqual(form.cleaned_data, {"handle": "foo"})


@override_settings(ROOT_URLCONF='webwhois.tests.urls', TEMPLATES=TEMPLATES)
class TestWhoisFormView(SimpleTestCase):
    def test_handle_required(self):
        response = self.client.post(reverse("webwhois:form_whois"))
        self.assertEqual(response.context["form"].errors, {'handle': ['This field is required.']})

    def test_handle_invalid(self):
        response = self.client.post(reverse("webwhois:form_whois"), {"handle": "a" * 256})
        self.assertEqual(response.context["form"].errors, {'handle': [
            'Ensure this value has at most 255 characters (it has 256).']})

    def test_handle_pattern(self):
        response = self.client.post(reverse("webwhois:form_whois"), {"handle": "a%3Fx"})
        self.assertRedirects(response, reverse("webwhois:registry_object_type", kwargs={"handle": "a%3Fx"}),
                             fetch_redirect_response=False)

    def test_valid_handle(self):
        response = self.client.post(reverse("webwhois:form_whois"), {"handle": " mycontact "})
        self.assertRedirects(response, reverse("webwhois:registry_object_type", kwargs={"handle": "mycontact"}),
                             fetch_redirect_response=False)

    def test_get_form(self):
        response = self.client.get(reverse("webwhois:form_whois"), {"handle": " mycontact "})
        self.assertContains(response, '<label for="id_handle">Domain (without <em>www.</em> prefix)'
                            ' / Handle:</label>', html=True)


class TestPublicRequestBaseForm(SimpleTestCase):
    """Test `PublicRequestBaseForm` class."""

    def test_clean_confirmation_method(self):
        form = PublicRequestBaseForm({'confirmation_method': 'signed_email'})
        form.is_valid()
        self.assertEqual(form.cleaned_data['confirmation_method'], ConfirmationMethod.SIGNED_EMAIL)

    def test_clean_confirmation_method_empty(self):
        form = PublicRequestBaseForm({})
        form.is_valid()
        self.assertIsNone(form.cleaned_data['confirmation_method'])

    def test_get_log_properties(self):
        data = (
            # post, properties
            ({'object_type': 'contact', 'handle': 'kryten'},
             {'handle': 'kryten', 'handleType': RegistryObjectType.CONTACT}),
            ({'object_type': 'contact', 'handle': 'kryten', 'confirmation_method': 'signed_email'},
             {'handle': 'kryten', 'handleType': RegistryObjectType.CONTACT, 'confirmMethod': 'signed_email'}),
        )
        for post, properties in data:
            with self.subTest(post=post):
                form = PublicRequestBaseForm(post)
                form.is_valid()
                self.assertEqual(form.get_log_properties(), properties)


class TestSendPasswordForm(SimpleTestCase):

    def test_is_valid(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "signed_email",
            "send_to_0": "email_in_registry",
        })
        self.assertEqual(form.errors, {})
        self.assertEqual(form.cleaned_data, {
            'object_type': 'domain',
            'handle': 'foo.cz',
            'send_to': DeliveryType('email_in_registry', ""),
            'confirmation_method': ConfirmationMethod.SIGNED_EMAIL,
        })

    def test_field_is_required(self):
        form = SendPasswordForm({})
        self.assertEqual(form.errors, {
            'object_type': ['This field is required.'],
            'handle': ['This field is required.'],
            'send_to': ['This field is required.'],
        })

    def test_max_length(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "o" * 256,
            "confirmation_method": "signed_email",
            "send_to_0": "email_in_registry",
        })
        self.assertEqual(form.errors, {'handle': ['Ensure this value has at most 255 characters (it has 256).']})

    def test_stripped_handle(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "  foo  ",
            "confirmation_method": "signed_email",
            "send_to_0": "email_in_registry",
        })
        self.assertEqual(form.errors, {})
        self.assertEqual(form.cleaned_data["handle"], "foo")

    def test_enum_types(self):
        form = SendPasswordForm({
            "object_type": "foo",
            "handle": "foo.cz",
            "confirmation_method": "foo",
            "send_to_0": "foo",
        })
        self.assertEqual(form.errors, {
            'confirmation_method': ['Select a valid choice. foo is not one of the available choices.'],
            'object_type': ['Select a valid choice. foo is not one of the available choices.'],
            'send_to': ['Select a valid choice. foo is not one of the available choices.'],
        })

    def test_invalid_custom_email(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "signed_email",
            "send_to_0": "email_in_registry",
            "send_to_1": "foo",
        })
        self.assertEqual(form.errors, {'send_to': ['Enter a valid email address.']})

    def test_custom_email_and_sent_to_registry(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "signed_email",
            "send_to_0": "email_in_registry",
            "send_to_1": "foo@foo.off",
        })
        self.assertEqual(form.errors, {
            'send_to': ['Option "Send to email in registry" is incompatible with custom email. '
                        'Please choose one of the two options.']
        })

    def test_sent_to_custom_email(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "signed_email",
            "send_to_0": "custom_email",
        })
        self.assertEqual(form.errors, {
            'send_to': ['Custom email is required as "Send to custom email" option is selected. Please fill it in.']
        })

    def test_notarized_letter_send_to_registry(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "notarized_letter",
            "send_to_0": "email_in_registry",
            "send_to_1": "",
        })
        self.assertEqual(form.errors, {
            '__all__': ['Letter with officially verified signature can be sent only to the custom email. '
                        'Please select "Send to custom email" and enter it.']
        })

    def test_notarized_letter_without_custom_email(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "notarized_letter",
            "send_to_0": "custom_email",
        })
        self.assertEqual(form.errors, {
            'send_to': ['Custom email is required as "Send to custom email" option is selected. Please fill it in.']
        })

    def test_notarized_letter(self):
        form = SendPasswordForm({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "notarized_letter",
            "send_to_0": "custom_email",
            "send_to_1": "foo@foo.off",
        })
        self.assertEqual(form.errors, {})
        self.assertEqual(form.cleaned_data, {
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": ConfirmationMethod.NOTARIZED_LETTER,
            "send_to": DeliveryType("custom_email", "foo@foo.off"),
        })

    def test_get_log_properties(self):
        data = (
            # post, properties
            ({'object_type': 'contact', 'handle': 'kryten', 'send_to_0': 'email_in_registry'},
             {'handle': 'kryten', 'handleType': RegistryObjectType.CONTACT, 'sendTo': 'email_in_registry'}),
            ({'object_type': 'contact', 'handle': 'kryten', 'send_to_0': 'custom_email',
              'send_to_1': 'kryten@example.org'},
             {'handle': 'kryten', 'handleType': RegistryObjectType.CONTACT, 'sendTo': 'custom_email',
              'customEmail': 'kryten@example.org'}),
        )
        for post, properties in data:
            with self.subTest(post=post):
                form = SendPasswordForm(post)
                form.is_valid()
                self.assertEqual(form.get_log_properties(), properties)


class BlockUnblockFormMixin:

    form_class: Union[Type[BlockObjectForm], Type[UnblockObjectForm]]

    def test_is_valid(self):
        form = self.form_class({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "signed_email",
            "lock_type": "transfer",
        })
        cast(SimpleTestCase, self).assertEqual(form.errors, {})
        cast(SimpleTestCase, self).assertEqual(form.cleaned_data, {
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": ConfirmationMethod.SIGNED_EMAIL,
            "lock_type": "transfer",
        })

    def test_field_is_required(self):
        form = self.form_class({})
        cast(SimpleTestCase, self).assertEqual(form.errors, {
            'object_type': ['This field is required.'],
            'handle': ['This field is required.'],
            'lock_type': ['This field is required.'],
        })

    def test_max_length(self):
        form = self.form_class({
            "object_type": "domain",
            "handle": "o" * 256,
            "confirmation_method": "signed_email",
            "lock_type": "transfer",
        })
        cast(SimpleTestCase, self).assertEqual(
            form.errors, {'handle': ['Ensure this value has at most 255 characters (it has 256).']})

    def test_stripped_handle(self):
        form = self.form_class({
            "object_type": "domain",
            "handle": "  foo  ",
            "confirmation_method": "signed_email",
            "lock_type": "transfer",
        })
        cast(SimpleTestCase, self).assertEqual(form.errors, {})
        cast(SimpleTestCase, self).assertEqual(form.cleaned_data["handle"], "foo")

    def test_enum_types(self):
        form = self.form_class({
            "object_type": "foo",
            "handle": "foo.cz",
            "confirmation_method": "foo",
            "lock_type": "foo",
        })
        cast(SimpleTestCase, self).assertEqual(form.errors, {
            'confirmation_method': ['Select a valid choice. foo is not one of the available choices.'],
            'object_type': ['Select a valid choice. foo is not one of the available choices.'],
            'lock_type': ['Select a valid choice. foo is not one of the available choices.'],
        })

    def test_is_valid_notarized_letter_transfer(self):
        form = self.form_class({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "notarized_letter",
            "lock_type": "transfer",
        })
        cast(SimpleTestCase, self).assertEqual(form.errors, {})
        cast(SimpleTestCase, self).assertEqual(form.cleaned_data, {
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": ConfirmationMethod.NOTARIZED_LETTER,
            "lock_type": "transfer",
        })

    def test_is_valid_notarized_letter_all(self):
        form = self.form_class({
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": "notarized_letter",
            "lock_type": "all",
        })
        cast(SimpleTestCase, self).assertEqual(form.errors, {})
        cast(SimpleTestCase, self).assertEqual(form.cleaned_data, {
            "object_type": "domain",
            "handle": "foo.cz",
            "confirmation_method": ConfirmationMethod.NOTARIZED_LETTER,
            "lock_type": "all",
        })

    def test_get_log_properties(self):
        data = (
            # post, properties
            ({'object_type': 'contact', 'handle': 'kryten', 'send_to_0': 'email_in_registry'},
             {'handle': 'kryten', 'handleType': RegistryObjectType.CONTACT, 'sendTo': 'email_in_registry'}),
            ({'object_type': 'contact', 'handle': 'kryten', 'send_to_0': 'custom_email',
              'send_to_1': 'kryten@example.org'},
             {'handle': 'kryten', 'handleType': RegistryObjectType.CONTACT, 'sendTo': 'custom_email',
              'customEmail': 'kryten@example.org'}),
        )
        for post, properties in data:
            with cast(SimpleTestCase, self).subTest(post=post):
                form = SendPasswordForm(post)
                form.is_valid()
                cast(SimpleTestCase, self).assertEqual(form.get_log_properties(), properties)


class PersonalInfoFormTest(SimpleTestCase):
    def test_get_log_properties(self):
        data = (
            # post, properties
            ({'handle': 'kryten', 'send_to_0': 'email_in_registry'},
             {'handle': 'kryten', 'sendTo': 'email_in_registry', 'handleType': RegistryObjectType.CONTACT}),
            ({'handle': 'kryten', 'send_to_0': 'custom_email', 'send_to_1': 'kryten@example.org'},
             {'handle': 'kryten', 'sendTo': 'custom_email', 'customEmail': 'kryten@example.org',
              'handleType': RegistryObjectType.CONTACT}),
        )
        for post, properties in data:
            with self.subTest(post=post):
                form = PersonalInfoForm(post)
                form.is_valid()
                self.assertEqual(form.get_log_properties(), properties)


class TestBlockObjectForm(BlockUnblockFormMixin, SimpleTestCase):
    form_class = BlockObjectForm

    def test_log_entry_type(self):
        data = (
            # data, log_entry_type
            ({'lock_type': 'transfer'}, PublicRequestsLogEntryType.BLOCK_TRANSFER),
            ({'lock_type': 'all'}, PublicRequestsLogEntryType.BLOCK_CHANGES),
        )
        for post, log_entry_type in data:
            with self.subTest(log_entry_type=log_entry_type):
                form = self.form_class(post)
                form.is_valid()
                self.assertEqual(form.log_entry_type, log_entry_type)


class TestUnblockObjectForm(BlockUnblockFormMixin, SimpleTestCase):
    form_class = UnblockObjectForm

    def test_log_entry_type(self):
        data = (
            # data, log_entry_type
            ({'lock_type': 'transfer'}, PublicRequestsLogEntryType.UNBLOCK_TRANSFER),
            ({'lock_type': 'all'}, PublicRequestsLogEntryType.UNBLOCK_CHANGES),
        )
        for post, log_entry_type in data:
            with self.subTest(log_entry_type=log_entry_type):
                form = self.form_class(post)
                form.is_valid()
                self.assertEqual(form.log_entry_type, log_entry_type)
