#!/usr/bin/python
#
# Copyright (C) 2023-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from http import HTTPStatus
from unittest.mock import call, patch

from django.test import SimpleTestCase
from django.test.utils import override_settings
from django.urls import reverse
from fred_types import ContactId, RegistrarId, RegistryObjectType
from grill.utils import TestLogEntry, TestLoggerClient
from regal import Contact, ObjectEvent, ObjectEvents, Registrar
from regal.exceptions import ContactDoesNotExist

from webwhois.constants import LOGGER_SERVICE, LogEntryType, LogResult

from .utils import TEMPLATES


@override_settings(ROOT_URLCONF='webwhois.tests.urls', TEMPLATES=TEMPLATES)
class DetailContactTest(SimpleTestCase):
    object_id = ContactId('2X4B')

    def setUp(self):
        patcher = patch('webwhois.views.detail_contact.CONTACT_CLIENT')
        self.addCleanup(patcher.stop)
        self.contact_mock = patcher.start()
        self.contact_mock.get_contact_state.return_value = {}

        registrar_patcher = patch('webwhois.views.detail_contact.REGISTRAR_CLIENT')
        self.addCleanup(registrar_patcher.stop)
        self.registrar_mock = registrar_patcher.start()
        self.registrar_mock.get_registrar_info.return_value = Registrar(
            registrar_id=RegistrarId("HOLLY"), registrar_handle="HOLLY", name='Holly')

        self.test_logger = TestLoggerClient()
        log_patcher = patch('webwhois.settings.LOGGER.client', new=self.test_logger)
        self.addCleanup(log_patcher.stop)
        log_patcher.start()

    def test_contact_not_found(self):
        self.contact_mock.get_contact_id.side_effect = ContactDoesNotExist

        response = self.client.get(reverse("webwhois:detail_contact", kwargs={"handle": "testhandle"}))

        self.assertContains(response, "Contact not found", status_code=HTTPStatus.NOT_FOUND)
        self.assertEqual(self.contact_mock.mock_calls, [call.get_contact_id('testhandle')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.NOT_FOUND, source_ip='127.0.0.1',
                                 input_properties={'handle': 'testhandle', 'handleType': RegistryObjectType.CONTACT})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())

    def test_contact(self):
        self.contact_mock.get_contact_id.return_value = self.object_id
        events = ObjectEvents(registered=ObjectEvent(registrar_handle='HOLLY'),
                              transferred=ObjectEvent(registrar_handle='HOLLY'))
        self.contact_mock.get_contact_info.return_value = Contact(
            contact_id=self.object_id, contact_handle='mycontact', sponsoring_registrar='HOLLY', events=events)

        response = self.client.get(reverse("webwhois:detail_contact", kwargs={"handle": "mycontact"}))

        self.assertContains(response, "Contact details")
        self.assertContains(response, "mycontact")
        calls = [call.get_contact_id('mycontact'), call.get_contact_info(self.object_id),
                 call.get_contact_state(self.object_id, internal=False)]
        self.assertEqual(self.contact_mock.mock_calls, calls)
        self.assertEqual(self.registrar_mock.mock_calls,
                         [call.get_registrar_info('HOLLY'), call.get_registrar_info('HOLLY')])

        # Check logger
        log_entry = TestLogEntry(LOGGER_SERVICE, LogEntryType.INFO, LogResult.SUCCESS, source_ip='127.0.0.1',
                                 input_properties={'handle': 'mycontact', 'handleType': RegistryObjectType.CONTACT},
                                 references={RegistryObjectType.CONTACT: self.object_id})
        self.assertEqual(self.test_logger.mock.mock_calls, log_entry.get_calls())
