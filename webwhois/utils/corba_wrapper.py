#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
"""Utilities for Corba."""
from datetime import datetime
from typing import Any, cast

from django.conf import settings
from django.utils import timezone
from django.utils.functional import SimpleLazyObject
from fred_idl import ccReg
from fred_idl.Registry import Buffer, IsoDate, IsoDateTime, PublicRequest
from pyfco import CorbaClient, CorbaClientProxy, CorbaNameServiceClient, CorbaRecoder
from pyfco.recoder import decode_iso_date, decode_iso_datetime

from webwhois.settings import WEBWHOIS_SETTINGS


class WebwhoisCorbaRecoder(CorbaRecoder):
    """Decodes whois specific structures.

    Decodes ISO date and time corba structures into date and datetime objects.
    Decodes Registry.Buffer into into bytes.
    """

    def __init__(self, coding: str = 'ascii'):
        super().__init__(coding)
        self.add_recode_function(Buffer, self._decode_buffer, self._identity)
        self.add_recode_function(ccReg._objref_FileDownload, self._identity, self._identity)
        self.add_recode_function(IsoDate, decode_iso_date, self._identity)
        self.add_recode_function(IsoDateTime, self._decode_iso_datetime, self._identity)

    def _decode_buffer(self, value: Buffer) -> bytes:
        """Decode `Registry.Buffer` struct into bytes."""
        return cast(bytes, value.data)

    def _decode_iso_datetime(self, value: str) -> datetime:
        """Decode `IsoDateTime` struct to datetime object with respect to the timezone settings."""
        result = cast(datetime, decode_iso_datetime(value))
        if not settings.USE_TZ:
            result = timezone.make_naive(result, timezone.get_default_timezone())
        return result


_CLIENT = CorbaNameServiceClient(host_port=WEBWHOIS_SETTINGS.CORBA_NETLOC, context_name=WEBWHOIS_SETTINGS.CORBA_CONTEXT)


def load_public_request_from_idl() -> Any:
    return _CLIENT.get_object('PublicRequest', PublicRequest.PublicRequestIntf)


_PUBLIC_REQUEST = SimpleLazyObject(load_public_request_from_idl)
PUBLIC_REQUEST = CorbaClientProxy(CorbaClient(_PUBLIC_REQUEST, WebwhoisCorbaRecoder('utf-8'),
                                              PublicRequest.INTERNAL_SERVER_ERROR))


def _backport_log_entry_id(log_entry_id: str) -> int:
    """Backport log entry id from new to old format."""
    return int(log_entry_id.partition('.')[0])
