#
# Copyright (C) 2015-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
from django import forms
from django.core.validators import MaxLengthValidator, RegexValidator
from django.urls.converters import StringConverter
from django.utils.functional import keep_lazy
from django.utils.safestring import SafeString, mark_safe
from django.utils.translation import gettext_lazy as _

# Backport ability to keep lazy objects to mark_safe to Django <=4.0.
mark_safe_lazy = keep_lazy(SafeString)(mark_safe)


class WhoisForm(forms.Form):
    """Whois form to enter HANDLE."""

    handle = forms.CharField(
        label=mark_safe_lazy(_("Domain (without <em>www.</em> prefix) / Handle")),
        required=True, validators=[RegexValidator('^' + StringConverter.regex + '$'), MaxLengthValidator(255)],
    )
