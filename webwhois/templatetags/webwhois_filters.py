#
# Copyright (C) 2015-2024  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
import re
import textwrap
from datetime import date
from typing import Dict, Optional, cast

import idna
from django import template
from django.template.defaultfilters import stringfilter
from django.utils.dateparse import parse_date as _parse_date
from django.utils.translation import gettext_lazy as _

from ..settings import WEBWHOIS_SETTINGS

register = template.Library()


@register.filter
def text_wrap(value: str, size: int) -> str:
    """Insert enters (LF) into the text on every position defined by the size. Value must not be None."""
    return "\n".join(textwrap.wrap(value, size))


SSN_TYPE = {
    'national_identity_number': _('Day of Birth'),
    'national_identity_card': _('ID card number'),
    'passport_number': _('Passport number'),
    'company_registration_number': _('VAT ID number'),
    'social_security_number': _('MPSV ID'),
    'birthdate': _('Day of Birth'),
}


@register.filter
@stringfilter
def contact_ssn_type_label(value: str) -> str:
    """Replace SSN type code by translated description."""
    return SSN_TYPE.get(value, "{}: {}".format(_('Unspecified type'), value)) if value else ''


@register.filter
@stringfilter
def idn_decode(value: str) -> str:
    """Decode handle into IDN."""
    try:
        return idna.decode(value)
    except idna.IDNAError:
        pass
    return value


@register.filter
@stringfilter
def add_scheme(value: str) -> str:
    """Add scheme (protocol) when missing in url."""
    if not re.match("https?://", value):
        return "http://" + value
    return value


@register.filter
@stringfilter
def strip_scheme(value: str) -> str:
    """Strip scheme (protocol) from url."""
    return re.sub("^https?://", "", value)


@register.filter
def state_flag_description(value: str) -> str:
    """Return description of the state flag."""
    return cast(Dict[str, str], WEBWHOIS_SETTINGS.STATE_FLAGS_DESCRIPTIONS).get(value, value)


@register.filter
def parse_date(value: str) -> Optional[date]:
    """Parse date from a string."""
    try:
        return _parse_date(value)
    except (TypeError, ValueError):
        return None
