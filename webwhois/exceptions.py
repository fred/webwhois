#
# Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
from django.http import Http404


class WebwhoisError(Http404):
    """Object was not found.

    Attributes:
        handle: A handle of an object.
        code: A specific error code.
        message: A human readable error message.
    """

    def __init__(self, handle: str, code: str, *, message: str):
        self.handle = handle
        self.code = code
        self.message = message
        super().__init__(message)
