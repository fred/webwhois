#
# Copyright (C) 2016-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.
#
import os
from functools import partial
from typing import Any, Dict, cast

from appsettings import AppSettings, DictSetting, FileSetting, Setting, StringSetting
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from filed import FileClient, Storage
from frgal import make_credentials
from frgal.aio import SyncGrpcProxy
from grill import Logger, get_logger_client
from regal import ContactClient, DomainClient, KeysetClient, NssetClient, RegistrarClient
from statementor import SyncStatementor
from typist import SecretaryClient

from .constants import LOGGER_SERVICE, PUBLIC_REQUESTS_LOGGER_SERVICE, LogResult, PublicRequestsLogResult

DEFAULT_STATE_FLAGS_DESCRIPTIONS = {
    'deleteCandidate': _('To be deleted'),
    'linked': _('Has relation to other records in the registry'),
    'serverBlocked': _('Administratively blocked'),
    'conditionallyIdentifiedContact': _('Contact is conditionally identified'),
    'identifiedContact': _('Contact is identified'),
    'validatedContact': _('Contact is validated'),
    'serverDeleteProhibited': _('Deletion forbidden'),
    'serverTransferProhibited': _('Sponsoring registrar change forbidden'),
    'serverUpdateProhibited': _('Update forbidden'),
    'serverRegistrantChangeProhibited': _('Registrant change forbidden'),
    'serverRenewProhibited': _('Registration renewal forbidden'),
    'expirationWarning': _('The domain expires in 30 days'),
    'deleteWarning': _('The domain will be deleted in 11 days'),
    'validationWarning1': _('The domain validation expires in 30 days'),
    'validationWarning2': _('The domain validation expires in 15 days'),
    'unguarded': _('The domain is 30 days after expiration'),
    'outzoneUnguarded': _('The domain is out of zone after 30 days in expiration state'),
    'nssetMissing': _("The domain doesn't have associated nsset"),
    'expired': _('Domain expired'),
    'outzone': _("The domain isn't generated in the zone"),
    'serverOutzoneManual': _('The domain is administratively kept out of zone'),
    'serverInzoneManual': _('The domain is administratively kept in zone'),
    'notValidated': _('Domain not validated'),
    'outzoneUnguardedWarning': _('The domain is to be out of zone soon.'),
    'serverContactNameChangeProhibited': _('Name update forbidden'),
    'serverContactOrganizationChangeProhibited': _('Organization update forbidden'),
    'serverContactIdentChangeProhibited': _('Ident update forbidden'),
    'serverContactPermanentAddressChangeProhibited': _('Permanent address update forbidden'),
    'serverLinkProhibited': _('Contact linking forbidden'),
    'premiumDomain': _('VIP domain'),
}


def timeout_validator(value: Any) -> None:
    """Validate timeouts - must contain a number or tuple with two numbers."""
    if isinstance(value, (float, int)):
        return
    if isinstance(value, tuple) and len(value) == 2 and all(isinstance(v, (float, int)) for v in value):
        return
    raise ValidationError('Value %(value)s must be a float, int or a tuple with 2 float or int items.',
                          params={'value': value})


class LoggerOptionsSetting(DictSetting):
    """Custom dict setting for logger options."""

    def transform(self, value: Dict[str, Any]) -> Dict[str, Any]:
        """Transform the credentials."""
        value = super().transform(value)
        if 'credentials' in value:
            value['credentials'] = make_credentials(**value['credentials'])
        return value


class MergeDictSetting(DictSetting):
    """Merge dict setting."""

    def transform(self, value: Dict) -> Dict:
        """Merge setting value with the defaults."""
        result = cast(Dict, self.default.copy())
        result.update(value)
        return result


class WebwhoisAppSettings(AppSettings):
    """Web whois settings."""

    CDNSKEY_NETLOC = StringSetting(default=None)
    CDNSKEY_SSL_CERT = FileSetting(default=None, mode=os.R_OK)
    CORBA_NETLOC = StringSetting(default=partial(os.environ.get, 'FRED_WEBWHOIS_NETLOC', 'localhost'))
    CORBA_CONTEXT = StringSetting(default='fred')
    FILEMAN_NETLOC = StringSetting(required=True)
    FILEMAN_SSL_CERT = FileSetting(default=None, mode=os.R_OK)
    LOGGER = StringSetting(default='grill.DummyLoggerClient')
    LOGGER_OPTIONS = LoggerOptionsSetting(default={})
    REGISTRY_NETLOC = StringSetting(required=True)
    REGISTRY_SSL_CERT = FileSetting(default=None, mode=os.R_OK)
    SECRETARY_URL = StringSetting(required=True)
    SECRETARY_AUTH = Setting()
    SECRETARY_TIMEOUT = Setting(default=3.05, validators=[timeout_validator])
    STATE_FLAGS_DESCRIPTIONS = MergeDictSetting(key_type=str, default=DEFAULT_STATE_FLAGS_DESCRIPTIONS)

    class Meta:
        setting_prefix = 'WEBWHOIS_'


WEBWHOIS_SETTINGS = WebwhoisAppSettings()

_CONTACT_CLIENT = ContactClient(WEBWHOIS_SETTINGS.REGISTRY_NETLOC,
                                make_credentials(WEBWHOIS_SETTINGS.REGISTRY_SSL_CERT))
CONTACT_CLIENT = SyncGrpcProxy(_CONTACT_CLIENT)
_DOMAIN_CLIENT = DomainClient(WEBWHOIS_SETTINGS.REGISTRY_NETLOC,
                              make_credentials(WEBWHOIS_SETTINGS.REGISTRY_SSL_CERT))
DOMAIN_CLIENT = SyncGrpcProxy(_DOMAIN_CLIENT)
_KEYSET_CLIENT = KeysetClient(WEBWHOIS_SETTINGS.REGISTRY_NETLOC,
                              make_credentials(WEBWHOIS_SETTINGS.REGISTRY_SSL_CERT))
KEYSET_CLIENT = SyncGrpcProxy(_KEYSET_CLIENT)
_NSSET_CLIENT = NssetClient(WEBWHOIS_SETTINGS.REGISTRY_NETLOC,
                            make_credentials(WEBWHOIS_SETTINGS.REGISTRY_SSL_CERT))
NSSET_CLIENT = SyncGrpcProxy(_NSSET_CLIENT)
_REGISTRAR_CLIENT = RegistrarClient(WEBWHOIS_SETTINGS.REGISTRY_NETLOC,
                                    make_credentials(WEBWHOIS_SETTINGS.REGISTRY_SSL_CERT))
REGISTRAR_CLIENT = SyncGrpcProxy(_REGISTRAR_CLIENT)
SECRETARY_CLIENT = SecretaryClient(WEBWHOIS_SETTINGS.SECRETARY_URL, auth=WEBWHOIS_SETTINGS.SECRETARY_AUTH,
                                   timeout=WEBWHOIS_SETTINGS.SECRETARY_TIMEOUT)

_LOGGER_CLIENT = get_logger_client(WEBWHOIS_SETTINGS.LOGGER, **WEBWHOIS_SETTINGS.LOGGER_OPTIONS)
LOGGER = Logger(_LOGGER_CLIENT, LOGGER_SERVICE, LogResult.ERROR)
PUBLIC_REQUESTS_LOGGER = Logger(_LOGGER_CLIENT, PUBLIC_REQUESTS_LOGGER_SERVICE, PublicRequestsLogResult.ERROR)
STATEMENTOR = SyncStatementor(
    secretary_client=SECRETARY_CLIENT,
    contact_client=_CONTACT_CLIENT,
    domain_client=_DOMAIN_CLIENT,
    keyset_client=_KEYSET_CLIENT,
    nsset_client=_NSSET_CLIENT,
    registrar_client=_REGISTRAR_CLIENT,
)
STORAGE = Storage(FileClient(WEBWHOIS_SETTINGS.FILEMAN_NETLOC, make_credentials(WEBWHOIS_SETTINGS.FILEMAN_SSL_CERT)))
